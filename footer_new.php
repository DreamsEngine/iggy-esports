            </div>
            <!--pinno-main-body-wrap-->
            <style>
                #pinno-foot-wrap {
                    padding-top: 0;
                }
                #pinno-foot-logo{
                    line-height: normal;
                }
                #pinno-foot-copy {
                    width: 100%;
                    padding-top: 0.4em;
                    text-align: right;
                }
                .top_footer {
                    padding: 1.5em 0 1em;
                    background: #191919;
                    margin-bottom: 2em;
                }

                .top_footer #pinno-foot-menu ul li a {
                    color: #fff;
                    font-family: 'Work Sans', sans-serif;
                    font-weight: 500;
                }

                .bottom_footer {
                    display: flex;
                    max-width: 1200px;
                    width: 90%;
                    flex-flow: row wrap;
                    justify-content: space-between;
                    margin: 2.5em auto 0;
                    font-weight: 400;
                    align-items: flex-start;
                }


                .bottom_footer h4,
                .bottom_footer h4 a {
                    color: white;
                    font-family: 'Merriweather', sans-serif !important;
                    text-transform: capitalize;
                    margin-bottom: 1.1em;
                }

                .footer_list li {
                    margin: .5em 0;
                }

                .footer_list li a {
                    font-family: 'Work Sans', sans-serif;
                    font-weight: 300;
                    color: #888;
                    text-align: left;
                    font-size: .9em;
                }

                #pinno-foot-logo img {
                    max-width: 140px;
                    margin: 0 auto;
                    display: block;
                }

                #pinno-foot-logo {
                    height: auto;
                    justify-content: flex-start;
                    margin-bottom: 15px;
                    flex-flow: column;
                }
                ul.pinno-foot-soc-list{
                    margin-top: 1.5em;
                    text-align: left;
                }
                ul.pinno-foot-soc-list li a {
                    margin-top: 0;
                    height: 40px;
                    margin-bottom: 1.3em;
                }
                .footer_block {
                        width: 25%;
                    }
                    .mobile_only{
                        display: none;
                    }
                    .no_mobile{
                        display: block;
                    }
                @media screen and (max-width:599px) {
                    ul.pinno-foot-soc-list li a{
                        height: 30px;
                    }
                    #pinno-foot-logo img{
                        margin: 0;
                    }
                    .footer_block {
                        width: 100%;
                        margin-bottom: 1em;
                        text-align: center;
                    }
                    .footer_block:first-of-type{
                        margin-bottom: 0;
                    }
                    .footer_block:nth-of-type(2) h4{
                        display: none;
                    }
                    #pinno-foot-logo img{
                        margin: 0 auto;
                    }
                    .mobile_only{
                        display: block;
                    }
                    .no_mobile{
                        display: none;
                    }
                    ul.pinno-foot-soc-list{
                        text-align: center;
                    }
                }
                @media screen and (min-width:600px) and (max-width:768px) {
                    .footer_block {
                        width: 50%;
                        margin-bottom: 2em;
                    }
                    #pinno-foot-logo img{
                        margin: 0;
                    }
                    ul.pinno-foot-soc-list,
                    #pinno-foot-logo {
                        text-align: left;
                    }

                }
            </style>

            <footer id="pinno-foot-wrap" class="left relative">
                <div class="top_footer">
                    <div id="pinno-foot-menu"><?php wp_nav_menu(array('theme_location' => 'footer-menu')); ?></div>
                </div>
                <div class="bottom_footer align-items-center ">
                    <div class="footer_block">
                    <div id="pinno-foot-soc" class="mobile_only">
                            <ul class="pinno-foot-soc-list left relative">
                                <?php if (get_option('pinno_facebook')) { ?>
                                    <li><a href="<?php echo esc_url(get_option('pinno_facebook')); ?>" target="_blank" class="fa fa-facebook fa-2"></a></li>
                                <?php } ?>
                                <?php if (get_option('pinno_twitter')) { ?>
                                    <li><a href="<?php echo esc_url(get_option('pinno_twitter')); ?>" target="_blank" class="fa fa-twitter fa-2"></a></li>
                                <?php } ?>
                                <?php if (get_option('pinno_pinterest')) { ?>
                                    <li><a href="<?php echo esc_url(get_option('pinno_pinterest')); ?>" target="_blank" class="fa fa-pinterest-p fa-2"></a></li>
                                <?php } ?>
                                <?php if (get_option('pinno_instagram')) { ?>
                                    <li><a href="<?php echo esc_url(get_option('pinno_instagram')); ?>" target="_blank" class="fa fa-instagram fa-2"></a></li>
                                <?php } ?>
                                <?php if (get_option('pinno_google')) { ?>
                                    <li><a href="<?php echo esc_url(get_option('pinno_google')); ?>" target="_blank" class="fa fa-google-plus fa-2"></a></li>
                                <?php } ?>
                                <?php if (get_option('pinno_youtube')) { ?>
                                    <li><a href="<?php echo esc_url(get_option('pinno_youtube')); ?>" target="_blank" class="fa fa-youtube-play fa-2"></a></li>
                                <?php } ?>
                                <?php if (get_option('pinno_linkedin')) { ?>
                                    <li><a href="<?php echo esc_url(get_option('pinno_linkedin')); ?>" target="_blank" class="fa fa-linkedin fa-2"></a></li>
                                <?php } ?>
                                <?php if (get_option('pinno_tumblr')) { ?>
                                    <li><a href="<?php echo esc_url(get_option('pinno_tumblr')); ?>" target="_blank" class="fa fa-tumblr fa-2"></a></li>
                                <?php } ?>
                            </ul>
                        </div>
                        <h4>secciones</h4>
                        <ul class="footer_list">
                            <li><a href="https://forbes.co/seccion/editors-picks/">Editor's Picks</a></li>
                            <li><a href="https://forbes.co/seccion/capital-humano/">Capital Humano</a></li>
                            <li><a href="https://forbes.co/seccion/economia-y-finanzas/">Economía y Finanzas</a></li>
                            <li><a href="https://forbes.co/seccion/emprendedores/">Emprendedores</a></li>
                            <li><a href="https://forbes.co/seccion/forbes-women/">Forbes Women</a></li>
                            <li><a href="https://forbes.co/seccion/tecnologia/">Tecnología</a></li>
                        </ul>
                        <div id="pinno-foot-soc" class="no_mobile">
                            <ul class="pinno-foot-soc-list left relative">
                                <?php if (get_option('pinno_facebook')) { ?>
                                    <li><a href="<?php echo esc_url(get_option('pinno_facebook')); ?>" target="_blank" class="fa fa-facebook fa-2"></a></li>
                                <?php } ?>
                                <?php if (get_option('pinno_twitter')) { ?>
                                    <li><a href="<?php echo esc_url(get_option('pinno_twitter')); ?>" target="_blank" class="fa fa-twitter fa-2"></a></li>
                                <?php } ?>
                                <?php if (get_option('pinno_pinterest')) { ?>
                                    <li><a href="<?php echo esc_url(get_option('pinno_pinterest')); ?>" target="_blank" class="fa fa-pinterest-p fa-2"></a></li>
                                <?php } ?>
                                <?php if (get_option('pinno_instagram')) { ?>
                                    <li><a href="<?php echo esc_url(get_option('pinno_instagram')); ?>" target="_blank" class="fa fa-instagram fa-2"></a></li>
                                <?php } ?>
                                <?php if (get_option('pinno_google')) { ?>
                                    <li><a href="<?php echo esc_url(get_option('pinno_google')); ?>" target="_blank" class="fa fa-google-plus fa-2"></a></li>
                                <?php } ?>
                                <?php if (get_option('pinno_youtube')) { ?>
                                    <li><a href="<?php echo esc_url(get_option('pinno_youtube')); ?>" target="_blank" class="fa fa-youtube-play fa-2"></a></li>
                                <?php } ?>
                                <?php if (get_option('pinno_linkedin')) { ?>
                                    <li><a href="<?php echo esc_url(get_option('pinno_linkedin')); ?>" target="_blank" class="fa fa-linkedin fa-2"></a></li>
                                <?php } ?>
                                <?php if (get_option('pinno_tumblr')) { ?>
                                    <li><a href="<?php echo esc_url(get_option('pinno_tumblr')); ?>" target="_blank" class="fa fa-tumblr fa-2"></a></li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                    <div class="footer_block">
                        <h4>&nbsp;</h4>
                        <ul class="footer_list">
                            <li><a href="https://forbes.co/seccion/negocios/">Negocios</a></li>
                            <li><a href="https://forbes.co/seccion/politica/">Política</a></li>
                            <li><a href="https://forbes.co/seccion/negocios/deportes">Negocios</a></li>
                            <li><a href="https://forbes.co/tag/heroes/">Héroes</a></li>
                            <li><a href="https://forbes.co/seccion/actualidad/">Actualidad</a></li>
                        </ul>
                    </div>
                    <div class="footer_block">
                        <h4><a href="https://forbes.co/seccion/brandvoice/">Brand Voice</a>
                        </h4>
                        <h4><a href="https://forbes.co/tag/coronavirus-colombia/">Coronavirus</a>
                        </h4>
                        <h4><a href="https://forbes.co/seccion/country-branding/">Country Branding</a>
                        </h4>
                        <ul class="footer_list">
                            <li><a href="https://forbes.co/seccion/country-branding/baranquilla-country-branding/">Barranquilla</a></li>
                            <li><a href="https://forbes.co/seccion/country-branding/panama-country-branding/">Panama</a></li>
                        </ul>
                    </div>
                    <div class="footer_block">
                        
                        <div id="pinno-foot-logo" class="left relative">
                            <h4>Revista del mes</h4>
                            <?php if (get_option('pinno_logo_foot')) { ?>
                                <a href="https://suscripciones.groupbrands.net/collections/forbes-en-espanol/products/12-ejemplares-forbes-colombia">
                                    <img src="<?php echo esc_url(get_option('pinno_logo_foot')); ?>" alt="<?php bloginfo('name'); ?>" data-rjs="2" />
                                </a>
                            <?php } else { ?>
                                <a href="<?php echo esc_url(home_url('/')); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/logos/logo-large.png" alt="<?php bloginfo('name'); ?>" data-rjs="2" /></a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div id="pinno-foot-bot" class="left relative">
                    <div class="pinno-main-box">
                        <div id="pinno-foot-copy" class="left relative">
                            <p><?php echo wp_kses_post(get_option('pinno_copyright')); ?></p>
                        </div>
                        <!--pinno-foot-copy-->
                    </div>
                    <!--pinno-main-box-->
                </div>
            </footer>
            </div>
            <!--pinno-site-main-->
            </div>
            <!--pinno-site-wall-->
            </div>
            <!--pinno-site-->
            <div class="pinno-fly-top back-to-top">
                <i class="fa fa-angle-up fa-3"></i>
            </div>
            <!--pinno-fly-top-->
            <div class="pinno-fly-fade pinno-fly-but-click">
            </div>
            <!--pinno-fly-fade-->
            <?php wp_footer(); ?>
            </body>

            </html>