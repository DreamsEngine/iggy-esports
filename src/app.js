/* eslint-disable no-console */

// components js
import DreamsEngine from "./components/plugins/den";
// import VideoGlider from "./components/home/home-videos";
import headerFunction from "./components/header/header";
import sidebarMenuHome from "./components/home/home-main-des";
import createAds from "./components/singles/ISAds";

// run variables
const isHome = document.body.classList.contains("home");
const isSingle = document.body.classList.contains("single");

// baseline app styles
require("./app/fonts/fonts.css");
require("./app/app.css");

// variables styles
require.context("./variables/", true, /\.css$/);

// components styles
require.context("./components/", true, /\.css$/);

DreamsEngine();
if (isHome === true) {
  sidebarMenuHome();
  headerFunction();
  // VideoGlider();
}

if (isSingle === true) {
  createAds();
  console.warn(`exist`);
} else {
  console.warn(`does not exist`);
}
