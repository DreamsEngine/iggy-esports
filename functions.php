<?php
/*---[ Details ]---------------------------------------
Function 1.0
Author: Adrian Galvez G.
Contact: adrian@dreamsengine.io
-------------------------------------------------------*/

/*-----------------------------------------------------
[00] Global Variables
[01] Base & Framwork Options
[02] Load Javascript
[03] Plugins
[04] Custom Post Types and Taxonomies
[05] Meta Box Panel
-------------------------------------------------------*/

/* [00] Global Variables
-------------------------------------------------------*/
$themename = "EsportsMx";
$prefix = "den_";
$denCDN = "https://cdn.devstate.de";
$denSiteURL = "https://dreamsengine.io";

/* Define Lib */
define('LIB_DIR', trailingslashit(get_stylesheet_directory(). '/lib'));

require_once LIB_DIR.'log.inc.php';

/* Define Includes */
define('INC_DIR', trailingslashit(get_stylesheet_directory(). '/inc'));

/* Define Metaboxes */
define('MB_DIR', trailingslashit(get_stylesheet_directory(). '/inc/plugins/meta-box'));
define('MB_ADDON_DIR', trailingslashit(get_stylesheet_directory(). '/inc/plugins/meta-box-plugins'));

/* Define Dreams Plugins*/
define( 'DREAMS_DIR', trailingslashit( get_stylesheet_directory(). '/inc/plugins/dreams' ) );
define( 'WIDGETS_DIR', trailingslashit( get_stylesheet_directory(). '/inc/widgets' ) );
define( 'WIDGETS_DIR2', trailingslashit( get_stylesheet_directory(). '/widgets' ) );



/* [01] Base & Framwork Options
-------------------------------------------------------*/
require_once INC_DIR .  'op/op-base.php';

/* [03] Dashboard & Framework Options
-------------------------------------------------------*/
require_once INC_DIR . "op/op-dashboard.php";

/* [04] Load JS$
-------------------------------------------------------*/
require_once INC_DIR .  'op/op-loader.php';

/* [05] Load JS$
-------------------------------------------------------*/
require_once INC_DIR .  'extras.php';

/* [06] Widgets
-------------------------------------------------------*/
require_once WIDGETS_DIR . 'widget-home-main.php';
require_once WIDGETS_DIR . 'widget-home-feat.php';
require_once WIDGETS_DIR . 'widget-home-video-sect.php';
require_once WIDGETS_DIR . 'widget-home-red-forbes.php';
require_once WIDGETS_DIR . 'widget-home-grids.php';



/* [07] Metaboxes
-------------------------------------------------------*/
require_once INC_DIR . 'op/op-metaboxes.php';







 
/**
 * Remove Custom Fields meta box
 */
//add_action( 'do_meta_boxes' , 'remove_post_custom_fields' );

function remove_post_custom_fields() {
  remove_meta_box( 'pinno-video-embed' , 'post' , 'normal' ); 
  remove_meta_box( 'pinno-featured-headline' , 'post' , 'normal' ); 
  remove_meta_box( 'pinno-photo-credit' , 'post' , 'normal' ); 
}

/* ====================================================== WIDGET ASUS---------------- */

// Creating the widget 
class pinno_home_feat_category_title extends WP_Widget {
  
function __construct() {
parent::__construct(
  
// Base ID of your widget
'pinno_home_feat_category_title', 
  
// Widget name will appear in UI
__('Featured category widget', 'pinno_home_feat_category_title_domain'), 
  
// Widget description
array( 'description' => __( 'Displays  3 posts in a row with a title floating left', 'pinno_home_feat_category_title_domain' ), ) 
);
}
  
// Creating widget front-end
  
public function widget( $args, $instance ) {
$title = apply_filters( 'widget_title', $instance['title'] );
  
// before and after widget arguments are defined by themes
echo $args['before_widget'];
if ( ! empty( $title ) )
//echo $args['before_title'] . $title . $args['after_title'];
  
// This is where you run the code and display the output

// EMPIEZA LOOP
echo '<div class="pinno-widget-home-head"><h4 class="pinno-widget-home-title"><span class="pinno-widget-home-title">acce</span></h4><h4 class="pinno-widget-home-title"><span class="pinno-widget-home-title">sorios</span></h4></div>';
echo '<div class="pinno-widget-feat1-wrap left relative">';
echo '<div class="pinno-widget-feat1-cont left relative">';

$query1 = new WP_Query( array( 'category_name' => 'accesorios', 'posts_per_page' => 3 ) );
while ( $query1->have_posts() ) { $query1->the_post();
  
  echo '<a href="';
  echo the_permalink();
  echo '" rel="bookmark">
              <div class="pinno-widget-feat1-bot-story left relative">
                <div class="pinno-widget-feat1-bot-img left relative">
                <img width="400" height="240" src="';
                echo get_the_post_thumbnail_url();
                echo '"class="pinno-reg-img lazy wp-post-image" alt="" loading="lazy" ><img width="80" height="80" src="';
                echo get_the_post_thumbnail_url();
                echo '"class="pinno-mob-img lazy wp-post-image" alt="" loading="lazy" srcset="';
                echo get_the_post_thumbnail_url();
                echo '" sizes="(max-width: 80px) 100vw, 80px"></div><!--pinno-widget-feat1-bot-img-->
                <div class="pinno-widget-feat1-bot-text left relative">
                  <div class="pinno-cat-date-wrap left relative">
                    <span class="pinno-cd-cat left relative">Accesorios</span><span class="pinno-cd-date left relative">2 days ago</span>
                  </div><!--pinno-cat-date-wrap-->
                  <h2>';
                  echo the_title( );
                  echo '</h2>
                </div><!--pinno-widget-feat1-bot-text-->
              </div><!--pinno-widget-feat1-bot-story-->
              </a>';
}
wp_reset_postdata();
echo '</div><!--pinno-widget-feat1-cont-->';
echo '</div><!--pinno-widget-feat1-wrap-->';

// ACABA LOOP
echo $args['after_widget'];
}
          
// Widget Backend 
public function form( $instance ) {
if ( isset( $instance[ 'title' ] ) ) {
$title = $instance[ 'title' ];
}
else {
$title = __( 'New title', 'pinno_home_feat_category_title_domain' );
}
// Widget admin form
?>
<p>
<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Category:' ); ?></label> 
<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
</p>
<?php 
}
      
// Updating widget replacing old instances with new
public function update( $new_instance, $old_instance ) {
$instance = array();
$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
return $instance;
}
 
// Class pinno_home_feat_category_title ends here
} 
 
 
// Register and load the widget
function wpb_load_widget() {
    register_widget( 'pinno_home_feat_category_title' );
}
add_action( 'widgets_init', 'wpb_load_widget' );