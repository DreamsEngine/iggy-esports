<?php
namespace MBUP;

class Helpers {
	/**
	 * Load template file name with priorities:
	 * - Child theme (inside folder /mb-frontend-submission/
	 * - Parent theme (inside folder /mb-frontend-submission/
	 * - Plugin /templates/ folder
	 *
	 * @param string $slug Template file slug.
	 * @param string $name Template variation.
	 * @param array  $data Data passed to template file.
	 */
	public static function load_template( $slug, $name = '', $data = array() ) {
		static $template_loader = null;
		if ( null === $template_loader ) {
			$template_loader = new TemplateLoader();
		}
		$template_loader->set_template_data( $data )->get_template_part( $slug, $name );
	}
}
