<?php
namespace MBUP\Shortcodes;

use RWMB_Helpers_Array as ArrayHelper;
use MBUP\Forms\Info as Form;
use MBUP\User;
use MBUP\Appearance;

class Info extends Base {
	/**
	 * Shortcode type.
	 *
	 * @var string
	 */
	protected $type = 'info';

	/**
	 * Get the form.
	 *
	 * @param array $args Form configuration.
	 *
	 * @return Form Form object.
	 */
	protected function get_form( $args ) {
		$args = shortcode_atts( array(
			// Meta Box ID.
			'id'                => '',

			// User fields.
			'user_id'           => get_current_user_id(),

			'redirect'          => '',
			'form_id'           => 'profile-form',

			// Appearance options.
			'label_password'    => __( 'New Password', 'mb-user-profile' ),
			'label_password2'   => __( 'Confirm Password', 'mb-user-profile' ),
			'label_submit'      => __( 'Submit', 'mb-user-profile' ),

			'id_password'       => 'user_pass',
			'id_password2'      => 'user_pass2',
			'id_submit'         => 'submit',

			'confirmation'      => __( 'Your information has been successfully submitted. Thank you.', 'mb-user-profile' ),

			'password_strength' => 'strong',
		), $args );

		// Compatible with old shortcode attributes.
		ArrayHelper::change_key( $args, 'submit_button', 'label_submit' );

		// Apply changes to appearance.
		$base_meta_box = rwmb_get_registry( 'meta_box' )->get( 'rwmb-user-info' );
		$appearance = new Appearance( $base_meta_box );

		$appearance->set( 'password.name', $args['label_password'] );
		$appearance->set( 'password.id', $args['id_password'] );

		$appearance->set( 'password2.name', $args['label_password2'] );
		$appearance->set( 'password2.id', $args['id_password2'] );

		$meta_boxes = array();
		$meta_box_ids = array_filter( array_map( 'trim', explode( ',', $args['id'] . ',' ) ) );

		foreach ( $meta_box_ids as $k => $meta_box_id ) {
			if ( 'rwmb-user-login' === $meta_box_id || 'rwmb-user-register' === $meta_box_id ) {
				unset( $meta_box_ids[$k] );
				continue;
			}
			$meta_box = rwmb_get_registry( 'meta_box' )->get( $meta_box_id );
			if ( ! $meta_box ) {
				unset( $meta_box_ids[$k] );
				continue;
			}
			$meta_box->set_object_id( $args['user_id'] );
			$meta_boxes[] = $meta_box;
		}
		if ( ! $meta_boxes ) {
			return null;
		}

		$args['id'] = implode( ',', $meta_box_ids );

		$user = new User( $args );
		$user->set_user_id( $args['user_id'] );

		return new Form( $meta_boxes, $user, $args );
	}
}
