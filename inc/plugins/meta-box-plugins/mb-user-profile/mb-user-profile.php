<?php
/**
 * Plugin Name: MB User Profile
 * Plugin URI:  https://metabox.io/plugins/mb-user-profile/
 * Description: Register, edit user profiles with custom fields on the front end.
 * Version:     1.4.2
 * Author:      MetaBox.io
 * Author URI:  https://metabox.io
 * License:     GPL2+
 * Text Domain: mb-user-profile
 * Domain Path: /languages/
 *
 * @package    Meta Box
 * @subpackage MB User Profile
 */

// Prevent loading this file directly.
defined( 'ABSPATH' ) || exit;

if ( ! function_exists( 'mb_user_profile_load' ) ) {
	require __DIR__ . '/vendor/autoload.php';

	/**
	 * Hook to 'init' with priority 5 to make sure all actions are registered before Meta Boxes runs.
	 */
	add_action( 'init', 'mb_user_profile_load', 5 );

	/**
	 * Load plugin files after Meta Box is loaded
	 */
	function mb_user_profile_load() {
		if ( ! defined( 'RWMB_VER' ) ) {
			return;
		}

		define( 'MB_USER_PROFILE_DIR', __DIR__ );
		list( , $url ) = RWMB_Loader::get_path( MB_USER_PROFILE_DIR );
		define( 'MB_USER_PROFILE_URL', $url );

		load_plugin_textdomain( 'mb-user-profile', false, basename( __DIR__ ) . '/languages' );
		require __DIR__ . '/mb-user-profile-fields.php';

		$info = new MBUP\Shortcodes\Info;
		$info->init();

		$register = new MBUP\Shortcodes\Register;
		$register->init();

		$login = new MBUP\Shortcodes\Login;
		$login->init();
	}
}
