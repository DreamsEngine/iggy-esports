<?php
// Google
// GTM
require_once INC_DIR . '/plugins/google/gtm.php';
// AMP
require_once INC_DIR . '/plugins/google/amp.init.php';
// DFP
require_once INC_DIR . '/plugins/google/dfp.php';

// MINIFY STYLES
if (!function_exists('minify')):
	function minify($who)
	{
		$buffer = '';

		$compress = $who;

		$buffer = $compress;
		// Remove comments
		$buffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $buffer);
		// Remove space after colons
		$buffer = str_replace(': ', ':', $buffer);
		// Remove whitespace
		$buffer = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $buffer);

		return $buffer;
	}
endif;

// Bring CSS INLINE
if (!function_exists('pinno_styles_method')) {
	function pinno_styles_method()
	{
		wp_enqueue_style('pinno-custom-style', get_stylesheet_uri());
		$wallad = get_option('pinno_wall_ad');
		$primarycolor = get_option('pinno_primary_color');
		$secondcolor = get_option('pinno_second_color');
		$topnavbg = get_option('pinno_top_nav_bg');
		$topnavtext = get_option('pinno_top_nav_text');
		$topnavhover = get_option('pinno_top_nav_hover');
		$botnavbg = get_option('pinno_bot_nav_bg');
		$botnavtext = get_option('pinno_bot_nav_text');
		$botnavhover = get_option('pinno_bot_nav_hover');
		$link = get_option('pinno_link_color');
		$link2 = get_option('pinno_link2_color');
		$featured_font = get_option('pinno_featured_font');
		$title_font = get_option('pinno_title_font');
		$heading_font = get_option('pinno_heading_font');
		$content_font = get_option('pinno_content_font');
		$para_font = get_option('pinno_para_font');
		$menu_font = get_option('pinno_menu_font');
		$pinno_customcss = get_option('pinno_customcss');
		$pinno_theme_options = "
    
    #pinno-wallpaper {
        background: url($wallad) no-repeat 50% 0;
        }
    
    #pinno-foot-copy a {
        color: $link;
        }
    
    #pinno-content-main p a,
    .pinno-post-add-main p a {
        box-shadow: inset 0 -4px 0 $link;
        }
    
    #pinno-content-main p a:hover,
    .pinno-post-add-main p a:hover {
        background: $link;
        }
    
    a,
    a:visited,
    .post-info-name a,
    .woocommerce .woocommerce-breadcrumb a {
        color: $link2;
        }
    
    #pinno-side-wrap a:hover {
        color: $link2;
        }
    
    .pinno-fly-top:hover,
    .pinno-vid-box-wrap,
    ul.pinno-soc-mob-list li.pinno-soc-mob-com {
        background: $primarycolor;
        }
    
    nav.pinno-fly-nav-menu ul li.menu-item-has-children:after,
    .pinno-feat1-left-wrap span.pinno-cd-cat,
    .pinno-widget-feat1-top-story span.pinno-cd-cat,
    .pinno-widget-feat2-left-cont span.pinno-cd-cat,
    .pinno-widget-dark-feat span.pinno-cd-cat,
    .pinno-widget-dark-sub span.pinno-cd-cat,
    .pinno-vid-wide-text span.pinno-cd-cat,
    .pinno-feat2-top-text span.pinno-cd-cat,
    .pinno-feat3-main-story span.pinno-cd-cat,
    .pinno-feat3-sub-text span.pinno-cd-cat,
    .pinno-feat4-main-text span.pinno-cd-cat,
    .woocommerce-message:before,
    .woocommerce-info:before,
    .woocommerce-message:before {
        color: $primarycolor;
        }
    
    #searchform input,
    .pinno-authors-name {
        border-bottom: 1px solid $primarycolor;
        }
    
    .pinno-fly-top:hover {
        border-top: 1px solid $primarycolor;
        border-left: 1px solid $primarycolor;
        border-bottom: 1px solid $primarycolor;
        }
    
    .woocommerce .widget_price_filter .ui-slider .ui-slider-handle,
    .woocommerce #respond input#submit.alt,
    .woocommerce a.button.alt,
    .woocommerce button.button.alt,
    .woocommerce input.button.alt,
    .woocommerce #respond input#submit.alt:hover,
    .woocommerce a.button.alt:hover,
    .woocommerce button.button.alt:hover,
    .woocommerce input.button.alt:hover {
        background-color: $primarycolor;
        }
    
    .woocommerce-error,
    .woocommerce-info,
    .woocommerce-message {
        border-top-color: $primarycolor;
        }
    
    ul.pinno-feat1-list-buts li.active span.pinno-feat1-list-but,
    span.pinno-widget-home-title,
    span.pinno-post-cat,
    span.pinno-feat1-pop-head {
        background: $secondcolor;
        }
    
    .woocommerce span.onsale {
        background-color: $secondcolor;
        }
    
    .pinno-widget-feat2-side-more-but,
    .woocommerce .star-rating span:before,
    span.pinno-prev-next-label,
    .pinno-cat-date-wrap .sticky {
        color: $secondcolor !important;
        }
    
    #pinno-main-nav-top,
    #pinno-fly-wrap,
    .pinno-soc-mob-right,
    #pinno-main-nav-small-cont {
        background: $topnavbg;
        }
    
    #pinno-main-nav-small .pinno-fly-but-wrap span,
    #pinno-main-nav-small .pinno-search-but-wrap span,
    .pinno-nav-top-left .pinno-fly-but-wrap span,
    #pinno-fly-wrap .pinno-fly-but-wrap span {
        background: $topnavtext;
        }
    
    .pinno-nav-top-right .pinno-nav-search-but,
    span.pinno-fly-soc-head,
    .pinno-soc-mob-right i,
    #pinno-main-nav-small span.pinno-nav-search-but,
    #pinno-main-nav-small .pinno-nav-menu ul li a  {
        color: $topnavtext;
        }
    
    #pinno-main-nav-small .pinno-nav-menu ul li.menu-item-has-children a:after {
        border-color: $topnavtext transparent transparent transparent;
        }
    
    #pinno-nav-top-wrap span.pinno-nav-search-but:hover,
    #pinno-main-nav-small span.pinno-nav-search-but:hover {
        color: $topnavhover;
        }
    
    #pinno-nav-top-wrap .pinno-fly-but-wrap:hover span,
    #pinno-main-nav-small .pinno-fly-but-wrap:hover span,
    span.pinno-woo-cart-num:hover {
        background: $topnavhover;
        }
    
    #pinno-main-nav-bot-cont {
        background: $botnavbg;
        }
    
    #pinno-nav-bot-wrap .pinno-fly-but-wrap span,
    #pinno-nav-bot-wrap .pinno-search-but-wrap span {
        background: $botnavtext;
        }
    
    #pinno-nav-bot-wrap span.pinno-nav-search-but,
    #pinno-nav-bot-wrap .pinno-nav-menu ul li a {
        color: $botnavtext;
        }
    
    #pinno-nav-bot-wrap .pinno-nav-menu ul li.menu-item-has-children a:after {
        border-color: $botnavtext transparent transparent transparent;
        }
    
    .pinno-nav-menu ul li:hover a {
        border-bottom: 5px solid $botnavhover;
        }
    
    #pinno-nav-bot-wrap .pinno-fly-but-wrap:hover span {
        background: $botnavhover;
        }
    
    #pinno-nav-bot-wrap span.pinno-nav-search-but:hover {
        color: $botnavhover;
        }
    
    body,
    .pinno-feat1-feat-text p,
    .pinno-feat2-top-text p,
    .pinno-feat3-main-text p,
    .pinno-feat3-sub-text p,
    #searchform input,
    .pinno-author-info-text,
    span.pinno-post-excerpt,
    .pinno-nav-menu ul li ul.sub-menu li a,
    nav.pinno-fly-nav-menu ul li a,
    .pinno-ad-label,
    span.pinno-feat-caption,
    .pinno-post-tags a,
    .pinno-post-tags a:visited,
    span.pinno-author-box-name a,
    #pinno-author-box-text p,
    .pinno-post-gallery-text p,
    ul.pinno-soc-mob-list li span,
    #comments,
    h3#reply-title,
    h2.comments,
    #pinno-foot-copy p,
    span.pinno-fly-soc-head,
    .pinno-post-tags-header,
    span.pinno-prev-next-label,
    span.pinno-post-add-link-but,
    #pinno-comments-button a,
    #pinno-comments-button span.pinno-comment-but-text,
    .woocommerce ul.product_list_widget span.product-title,
    .woocommerce ul.product_list_widget li a,
    .woocommerce #reviews #comments ol.commentlist li .comment-text p.meta,
    .woocommerce div.product p.price,
    .woocommerce div.product p.price ins,
    .woocommerce div.product p.price del,
    .woocommerce ul.products li.product .price del,
    .woocommerce ul.products li.product .price ins,
    .woocommerce ul.products li.product .price,
    .woocommerce #respond input#submit,
    .woocommerce a.button,
    .woocommerce button.button,
    .woocommerce input.button,
    .woocommerce .widget_price_filter .price_slider_amount .button,
    .woocommerce span.onsale,
    .woocommerce-review-link,
    #woo-content p.woocommerce-result-count,
    .woocommerce div.product .woocommerce-tabs ul.tabs li a,
    a.pinno-inf-more-but,
    span.pinno-cont-read-but,
    span.pinno-cd-cat,
    span.pinno-cd-date,
    .pinno-feat4-main-text p,
    span.pinno-woo-cart-num,
    span.pinno-widget-home-title2,
    .wp-caption,
    #pinno-content-main p.wp-caption-text,
    .gallery-caption,
    .pinno-post-add-main p.wp-caption-text,
    #bbpress-forums,
    #bbpress-forums p,
    .protected-post-form input,
    #pinno-feat6-text p {
        font-family: '$content_font', sans-serif;
        }
    
    .pinno-blog-story-text p,
    span.pinno-author-page-desc,
    #pinno-404 p,
    .pinno-widget-feat1-bot-text p,
    .pinno-widget-feat2-left-text p,
    .pinno-flex-story-text p,
    .pinno-search-text p,
    #pinno-content-main p,
    .pinno-post-add-main p,
    #pinno-content-main ul li,
    #pinno-content-main ol li,
    .rwp-summary,
    .rwp-u-review__comment,
    .pinno-feat5-mid-main-text p,
    .pinno-feat5-small-main-text p,
    #pinno-content-main .wp-block-button__link,
    .wp-block-audio figcaption,
    .wp-block-video figcaption,
    .wp-block-embed figcaption,
    .wp-block-verse pre,
    pre.wp-block-verse {
        font-family: '$para_font', sans-serif;
        }
    
    .pinno-nav-menu ul li a,
    #pinno-foot-menu ul li a {
        font-family: '$menu_font', sans-serif;
        }
    
    
    .pinno-feat1-sub-text h2,
    .pinno-feat1-pop-text h2,
     h2,
    .pinno-widget-feat1-top-text h2,
    .pinno-widget-feat1-bot-text h2,
    .pinno-widget-dark-feat-text h2,
    .pinno-widget-dark-sub-text h2,
    .pinno-widget-feat2-left-text h2,
    .pinno-widget-feat2-right-text h2,
    .pinno-blog-story-text h2,
    .pinno-flex-story-text h2,
    .pinno-vid-wide-more-text p,
    .pinno-prev-next-text p,
    .pinno-related-text,
    .pinno-post-more-text p,
    h2.pinno-authors-latest a,
    .pinno-feat2-bot-text h2,
    .pinno-feat3-sub-text h2,
    .pinno-feat3-main-text h2,
    .pinno-feat4-main-text h2,
    .pinno-feat5-text h2,
    .pinno-feat5-mid-main-text h2,
    .pinno-feat5-small-main-text h2,
    .pinno-feat5-mid-sub-text h2,
    #pinno-feat6-text h2,
    .alp-related-posts-wrapper .alp-related-post .post-title {
        font-family: '$featured_font', sans-serif !important;
        }
    
    .pinno-feat2-top-text h2,
    .pinno-feat1-feat-text h2,
    h1.pinno-post-title,
    h1.pinno-post-title-wide,
    .pinno-drop-nav-title h4,
    #pinno-content-main blockquote p,
    .pinno-post-add-main blockquote p,
    #pinno-content-main p.has-large-font-size,
    #pinno-404 h1,
    #woo-content h1.page-title,
    .woocommerce div.product .product_title,
    .woocommerce ul.products li.product h3,
    .alp-related-posts .current .post-title {
        font-family: '$title_font', sans-serif !important;
        }
    
    span.pinno-feat1-pop-head,
    .pinno-feat1-pop-text:before,
    span.pinno-feat1-list-but,
    span.pinno-widget-home-title,
    .pinno-widget-feat2-side-more,
    span.pinno-post-cat,
    span.pinno-page-head,
    h1.pinno-author-top-head,
    .pinno-authors-name,
    #pinno-content-main h1,
    #pinno-content-main h2,
    #pinno-content-main h3,
    #pinno-content-main h4,
    #pinno-content-main h5,
    #pinno-content-main h6,
    .woocommerce .related h2,
    .woocommerce div.product .woocommerce-tabs .panel h2,
    .woocommerce div.product .product_title,
    .pinno-feat5-side-list .pinno-feat1-list-img:after {
        font-family: '$heading_font', sans-serif;
        }
    
        ";

		if (get_option('pinno_wall_ad')) {
			$pinno_wall_ad_css = "
        @media screen and (min-width: 1200px) {
        #pinno-site {
            float: none;
            margin: 0 auto;
            width: 1200px;
            }
        #pinno-leader-wrap {
            left: auto;
            width: 1200px;
            }
        .pinno-main-box {
            width: 1160px;
            }
        #pinno-main-nav-top,
        #pinno-main-nav-bot,
        #pinno-main-nav-small {
            width: 1200px;
            }
        }
            ";
		}

		$pinno_site_skin = get_option('pinno_site_skin');
		if ($pinno_site_skin == '1') {
			$pinno_site_skin_css = "
        #pinno-main-nav-top {
            background: #fff;
            padding: 15px 0 0;
            }
        #pinno-fly-wrap,
        .pinno-soc-mob-right,
        #pinno-main-nav-small-cont {
            background: #fff;
            }
        #pinno-main-nav-small .pinno-fly-but-wrap span,
        #pinno-main-nav-small .pinno-search-but-wrap span,
        .pinno-nav-top-left .pinno-fly-but-wrap span,
        #pinno-fly-wrap .pinno-fly-but-wrap span {
            background: #000;
            }
        .pinno-nav-top-right .pinno-nav-search-but,
        span.pinno-fly-soc-head,
        .pinno-soc-mob-right i,
        #pinno-main-nav-small span.pinno-nav-search-but,
        #pinno-main-nav-small .pinno-nav-menu ul li a  {
            color: #000;
            }
        #pinno-main-nav-small .pinno-nav-menu ul li.menu-item-has-children a:after {
            border-color: #000 transparent transparent transparent;
            }
        .pinno-feat1-feat-text h2,
        h1.pinno-post-title,
        .pinno-feat2-top-text h2,
        .pinno-feat3-main-text h2,
        #pinno-content-main blockquote p,
        .pinno-post-add-main blockquote p {
            font-family: 'Anton', sans-serif;
            font-weight: 400;
            letter-spacing: normal;
            }
        .pinno-feat1-feat-text h2,
        .pinno-feat2-top-text h2,
        .pinno-feat3-main-text h2 {
            line-height: 1;
            text-transform: uppercase;
            }
            ";
		}

		$pinno_nav_skin = get_option('pinno_nav_skin');
		$pinno_site_skin = get_option('pinno_site_skin');
		if ($pinno_nav_skin == '1' || $pinno_site_skin == '1') {
			$pinno_nav_skin_css = "
        span.pinno-nav-soc-but,
        ul.pinno-fly-soc-list li a,
        span.pinno-woo-cart-num {
            background: rgba(0,0,0,.8);
            }
        span.pinno-woo-cart-icon {
            color: rgba(0,0,0,.8);
            }
        nav.pinno-fly-nav-menu ul li,
        nav.pinno-fly-nav-menu ul li ul.sub-menu {
            border-top: 1px solid rgba(0,0,0,.1);
            }
        nav.pinno-fly-nav-menu ul li a {
            color: #000;
            }
        .pinno-drop-nav-title h4 {
            color: #000;
            }
            ";
		}

		$pinno_nav_layout = get_option('pinno_nav_layout');
		if ($pinno_nav_layout == '1') {
			$pinno_nav_layout_css = "
        #pinno-main-body-wrap {
            padding-top: 20px;
            }
        #pinno-feat2-wrap,
        #pinno-feat4-wrap,
        #pinno-post-feat-img-wide,
        #pinno-vid-wide-wrap {
            margin-top: -20px;
            }
        @media screen and (max-width: 479px) {
            #pinno-main-body-wrap {
                padding-top: 15px;
                }
            #pinno-feat2-wrap,
            #pinno-feat4-wrap,
            #pinno-post-feat-img-wide,
            #pinno-vid-wide-wrap {
                margin-top: -15px;
                }
            }
            ";
		}

		$pinno_prime_skin = get_option('pinno_prime_skin');
		if ($pinno_prime_skin == '1') {
			$pinno_prime_skin_css = "
        .pinno-vid-box-wrap,
        .pinno-feat1-left-wrap span.pinno-cd-cat,
        .pinno-widget-feat1-top-story span.pinno-cd-cat,
        .pinno-widget-feat2-left-cont span.pinno-cd-cat,
        .pinno-widget-dark-feat span.pinno-cd-cat,
        .pinno-widget-dark-sub span.pinno-cd-cat,
        .pinno-vid-wide-text span.pinno-cd-cat,
        .pinno-feat2-top-text span.pinno-cd-cat,
        .pinno-feat3-main-story span.pinno-cd-cat {
            color: #fff;
            }
            ";
		}

		$pinno_para_lead = get_option('pinno_para_lead');
		if ($pinno_para_lead == 'true') {
			if (isset($pinno_para_lead)) {
			}
		} else {
			$pinno_para_lead_css = "
        #pinno-leader-wrap {
            position: relative;
            }
        #pinno-site-main {
            margin-top: 0;
            }
        #pinno-leader-wrap {
            top: 0 !important;
            }
            ";
		}

		$pinno_infinite_scroll = get_option('pinno_infinite_scroll');
		if ($pinno_infinite_scroll == 'true') {
			if (isset($pinno_infinite_scroll)) {
				$pinno_infinite_scroll_css = "
        .pinno-nav-links {
            display: none;
            }
            ";
			}
		}

		$pinno_respond = get_option('pinno_respond');
		if ($pinno_respond == 'true') {
			if (isset($pinno_respond)) {
			}
		} else {
			$pinno_respond_css = "
        #pinno-site,
        #pinno-main-nav-top,
        #pinno-main-nav-bot {
            min-width: 1240px;
            }
            ";
		}

		$pinno_alp = get_option('pinno_alp');
		if ($pinno_alp !== 'true') {
			$pinno_cont_read = get_option('pinno_cont_read');
			if ($pinno_cont_read == 'true') {
				if (isset($pinno_cont_read)) {
					$pinno_cont_read_css = "
        @media screen and (max-width: 479px) {
            .single #pinno-content-body-top {
                max-height: 400px;
                }
            .single .pinno-cont-read-but-wrap {
                display: inline;
                }
            }
            ";
				}
			}
		}

		$pinno_rtl = get_option('pinno_rtl');
		if ($pinno_rtl == 'true') {
			global $post;
			if (!empty($post)) {
				$pinno_post_layout = get_option('pinno_post_layout');
				$pinno_post_temp = get_post_meta($post->ID, 'pinno_post_template', true);
				if (
					(empty($pinno_post_temp) && $pinno_post_layout == '1') ||
					($pinno_post_temp == 'def' && $pinno_post_layout == '1') ||
					(empty($pinno_post_temp) && $pinno_post_layout == '7') ||
					($pinno_post_temp == 'def' && $pinno_post_layout == '7') ||
					($pinno_post_temp == 'global' && $pinno_post_layout == '1') ||
					($pinno_post_temp == 'global' && $pinno_post_layout == '7') ||
					$pinno_post_temp == 'temp2' ||
					$pinno_post_temp == 'temp8'
				) {
					$pinno_post_side_css = "
        .pinno-post-main-out,
        .pinno-post-main-in {
            margin-left: 0 !important;
            }
        #pinno-post-feat-img img {
            width: 100%;
            }
        #pinno-content-wrap,
        #pinno-post-add-box {
            float: none;
            margin: 0 auto;
            max-width: 750px;
            }
            ";
				}
			}
		} else {
			global $post;
			if (!empty($post)) {
				$pinno_post_layout = get_option('pinno_post_layout');
				$pinno_post_temp = get_post_meta($post->ID, 'pinno_post_template', true);
				if (
					(empty($pinno_post_temp) && $pinno_post_layout == '1') ||
					($pinno_post_temp == 'def' && $pinno_post_layout == '1') ||
					(empty($pinno_post_temp) && $pinno_post_layout == '7') ||
					($pinno_post_temp == 'def' && $pinno_post_layout == '7') ||
					($pinno_post_temp == 'global' && $pinno_post_layout == '1') ||
					($pinno_post_temp == 'global' && $pinno_post_layout == '7') ||
					$pinno_post_temp == 'temp2' ||
					$pinno_post_temp == 'temp8'
				) {
					$pinno_post_side_css = "
        .pinno-post-main-out,
        .pinno-post-main-in {
            margin-right: 0 !important;
            }
        #pinno-post-feat-img img {
            width: 100%;
            }
        #pinno-content-wrap,
        #pinno-post-add-box {
            float: none;
            margin: 0 auto;
            max-width: 750px;
            }
            ";
				}
			}
		}

		$pinno_rtl = get_option('pinno_rtl');
		if ($pinno_rtl == 'true') {
			global $post;
			if (!empty($post)) {
				$pinno_post_layout = get_option('pinno_post_layout');
				$pinno_post_temp = get_post_meta($post->ID, 'pinno_post_template', true);
				if (
					(empty($pinno_post_temp) && $pinno_post_layout == '3') ||
					($pinno_post_temp == 'def' && $pinno_post_layout == '3') ||
					(empty($pinno_post_temp) && $pinno_post_layout == '5') ||
					($pinno_post_temp == 'def' && $pinno_post_layout == '5') ||
					($pinno_post_temp == 'global' && $pinno_post_layout == '3') ||
					($pinno_post_temp == 'global' && $pinno_post_layout == '5') ||
					$pinno_post_temp == 'temp4' ||
					$pinno_post_temp == 'temp6'
				) {
					$pinno_post_side2_css = "
        .pinno-post-main-out,
        .pinno-post-main-in {
            margin-left: 0 !important;
            }
        #pinno-post-feat-img img {
            width: 100%;
            }
        #pinno-post-content,
        #pinno-post-add-box {
            float: none;
            margin: 0 auto;
            max-width: 750px;
            }
            ";
				}
			}
		} else {
			global $post;
			if (!empty($post)) {
				$pinno_post_layout = get_option('pinno_post_layout');
				$pinno_post_temp = get_post_meta($post->ID, 'pinno_post_template', true);
				if (
					(empty($pinno_post_temp) && $pinno_post_layout == '3') ||
					($pinno_post_temp == 'def' && $pinno_post_layout == '3') ||
					(empty($pinno_post_temp) && $pinno_post_layout == '5') ||
					($pinno_post_temp == 'def' && $pinno_post_layout == '5') ||
					($pinno_post_temp == 'global' && $pinno_post_layout == '3') ||
					($pinno_post_temp == 'global' && $pinno_post_layout == '5') ||
					$pinno_post_temp == 'temp4' ||
					$pinno_post_temp == 'temp6'
				) {
					$pinno_post_side2_css = "
        .pinno-post-main-out,
        .pinno-post-main-in {
            margin-right: 0 !important;
            }
        #pinno-post-feat-img img {
            width: 100%;
            }
        #pinno-post-content,
        #pinno-post-add-box {
            float: none;
            margin: 0 auto;
            max-width: 750px;
            }
            ";
				}
			}
		}

		if (is_single()) {
			$pinno_rtl = get_option('pinno_rtl');
			if ($pinno_rtl == 'true') {
				global $post;
				if (!empty($post)) {
					$pinno_post_layout = get_option('pinno_post_layout');
					$pinno_post_temp = get_post_meta($post->ID, 'pinno_post_template', true);
					if (
						(empty($pinno_post_temp) && $pinno_post_layout == '4') ||
						($pinno_post_temp == 'def' && $pinno_post_layout == '4') ||
						(empty($pinno_post_temp) && $pinno_post_layout == '5') ||
						($pinno_post_temp == 'def' && $pinno_post_layout == '5') ||
						($pinno_post_temp == 'global' && $pinno_post_layout == '4') ||
						($pinno_post_temp == 'global' && $pinno_post_layout == '5') ||
						$pinno_post_temp == 'temp5' ||
						$pinno_post_temp == 'temp6'
					) {
						$pinno_post_side3_css = "
        .pinno-nav-soc-wrap {
            margin-top: -15px;
            height: 30px;
            }
        span.pinno-nav-soc-but {
            font-size: 16px;
            padding-top: 7px;
            width: 30px;
            height: 23px;
            }
        #pinno-main-nav-top {
            padding: 10px 0 !important;
            height: 30px !important;
            z-index: 9999;
            }
        .pinno-nav-top-wrap,
        .pinno-nav-top-mid {
            height: 30px !important;
            }
        .pinno-nav-top-mid img {
            height: 100% !important;
            }
        #pinno-main-nav-bot {
            border-bottom: none;
            display: none;
            height: 0;
            }
        .pinno-nav-top-mid img {
            margin-right: 0;
            }
        .pinno-nav-top-left-out {
            margin-right: -200px;
            }
        .pinno-nav-top-left-in {
            margin-right: 200px;
            }
        .pinno-nav-top-left {
            display: block;
            }
            ";
					}
				}
			} else {
				global $post;
				if (!empty($post)) {
					$pinno_post_layout = get_option('pinno_post_layout');
					$pinno_post_temp = get_post_meta($post->ID, 'pinno_post_template', true);
					if (
						(empty($pinno_post_temp) && $pinno_post_layout == '4') ||
						($pinno_post_temp == 'def' && $pinno_post_layout == '4') ||
						(empty($pinno_post_temp) && $pinno_post_layout == '5') ||
						($pinno_post_temp == 'def' && $pinno_post_layout == '5') ||
						($pinno_post_temp == 'global' && $pinno_post_layout == '4') ||
						($pinno_post_temp == 'global' && $pinno_post_layout == '5') ||
						$pinno_post_temp == 'temp5' ||
						$pinno_post_temp == 'temp6'
					) {
						$pinno_post_side3_css = "
        .pinno-nav-soc-wrap {
            margin-top: -15px;
            height: 30px;
            }
        span.pinno-nav-soc-but {
            font-size: 16px;
            padding-top: 7px;
            width: 30px;
            height: 23px;
            }
        #pinno-main-nav-top {
            padding: 10px 0 !important;
            height: 30px !important;
            z-index: 9999;
            }
        .pinno-nav-top-wrap,
        .pinno-nav-top-mid {
            height: 30px !important;
            }
        .pinno-nav-top-mid img {
            height: 100% !important;
            }
        #pinno-main-nav-bot {
            border-bottom: none;
            display: none;
            height: 0;
            }
        .pinno-nav-top-mid img {
            margin-left: 0;
            }
        .pinno-nav-top-left-out {
            margin-left: -200px;
            }
        .pinno-nav-top-left-in {
            margin-left: 200px;
            }
        .pinno-nav-top-left {
            display: block;
            }
            ";
					}
				}
			}
		}

		$pinno_alp = get_option('pinno_alp');
		$pinno_alp_side = get_option('pinno_alp_side');
		if ($pinno_alp == 'true') {
			if (isset($pinno_alp)) {
				if ($pinno_alp_side == '0') {
					$pinno_alp_css = "
        .pinno-auto-post-grid {
            grid-template-columns: 340px minmax(0, auto);
        }
            ";
				} elseif ($pinno_alp_side == '1') {
					$pinno_alp_css = "
        .pinno-alp-side {
            display: none;
        }
        .pinno-alp-soc-reg {
            display: block;
        }
        .pinno-auto-post-grid {
            grid-template-columns: minmax(0, auto) 320px;
            grid-column-gap: 60px;
        }
        @media screen and (max-width: 1199px) {
            .pinno-auto-post-grid {
                grid-column-gap: 30px;
            }
        }
            ";
				} else {
					$pinno_alp_css = "
        .pinno-alp-side {
            display: none;
        }
        .pinno-alp-soc-reg {
            display: block;
        }
        .pinno-auto-post-grid {
            grid-template-columns: 100%;
            margin: 30px auto 0;
            max-width: 1000px;
        }
        .pinno-auto-post-main #pinno-content-body {
            float: none;
            margin: 0 auto;
            max-width: 740px;
        }
            ";
				}
			}
		}

		$pinno_alp_ad = get_option('pinno_alp_ad');
		if (!$pinno_alp_ad) {
			$pinno_alp_side_ad_css = "
        .alp-advert {
            display: none;
        }
        .alp-related-posts-wrapper .alp-related-posts .current {
            margin: 0 0 10px;
        }
            ";
		}

		if ($pinno_customcss) {
			$pinno_customcss_css = "
         $pinno_customcss
            ";
		}

		if (isset($pinno_theme_options)) {
			wp_kses_post(wp_add_inline_style('pinno-custom-style', minify($pinno_theme_options)));
		}
		if (isset($pinno_wall_ad_css)) {
			wp_kses_post(wp_add_inline_style('pinno-custom-style', $pinno_wall_ad_css));
		}
		if (isset($pinno_prime_skin_css)) {
			wp_kses_post(wp_add_inline_style('pinno-custom-style', minify($pinno_prime_skin_css)));
		}
		if (isset($pinno_site_skin_css)) {
			wp_kses_post(wp_add_inline_style('pinno-custom-style', minify($pinno_site_skin_css)));
		}
		if (isset($pinno_nav_skin_css)) {
			wp_kses_post(wp_add_inline_style('pinno-custom-style', minify($pinno_nav_skin_css)));
		}
		if (isset($pinno_nav_layout_css)) {
			wp_kses_post(wp_add_inline_style('pinno-custom-style', minify($pinno_nav_layout_css)));
		}
		if (isset($pinno_para_lead_css)) {
			wp_kses_post(wp_add_inline_style('pinno-custom-style', minify($pinno_para_lead_css)));
		}
		if (isset($pinno_infinite_scroll_css)) {
			wp_kses_post(wp_add_inline_style('pinno-custom-style', minify($pinno_infinite_scroll_css)));
		}
		if (isset($pinno_respond_css)) {
			wp_kses_post(wp_add_inline_style('pinno-custom-style', $pinno_respond_css));
		}
		if (isset($pinno_cont_read_css)) {
			wp_kses_post(wp_add_inline_style('pinno-custom-style', minify($pinno_cont_read_css)));
		}
		if (isset($pinno_post_side_css)) {
			wp_kses_post(wp_add_inline_style('pinno-custom-style', minify($pinno_post_side_css)));
		}
		if (isset($pinno_post_side2_css)) {
			wp_kses_post(wp_add_inline_style('pinno-custom-style', minify($pinno_post_side2_css)));
		}
		if (isset($pinno_post_side3_css)) {
			wp_kses_post(wp_add_inline_style('pinno-custom-style', minify($pinno_post_side3_css)));
		}
		if (isset($pinno_alp_css)) {
			wp_kses_post(wp_add_inline_style('pinno-custom-style', minify($pinno_alp_css)));
		}
		if (isset($pinno_alp_side_ad_css)) {
			wp_kses_post(wp_add_inline_style('pinno-custom-style', minify($pinno_alp_side_ad_css)));
		}
		if (isset($pinno_customcss_css)) {
			wp_kses_post(wp_add_inline_style('pinno-custom-style', minify($pinno_customcss_css)));
		}
	}
}

add_action('wp_enqueue_scripts', 'pinno_styles_method');

/* First post functions */


//Excerpt Expert
if (!function_exists('excerpt')) :
    function excerpt($limit)
    {
//  $excerpt = explode(' ', get_the_excerpt(), $limit);
        $excerpt = get_the_excerpt();

        $excerpt = preg_replace('`\[[^\]]*\]`', '', $excerpt);
        $excerpt = preg_replace('/PUBLICIDAD/', '', $excerpt);

        if(strlen($excerpt) > $limit) {
            $excerpt = wp_html_excerpt($excerpt, $limit, $more = null) . '...';
        }

        return $excerpt;

        /*if ($num_words = 172) {
            $excerpt = wp_html_excerpt(get_the_excerpt(), $num_words, $more = null) . '...';

        } else if ($num_words < 172 ) {
            $excerpt = wp_html_excerpt(get_the_excerpt(), $limit, $more = null);
        }

        $excerpt = preg_replace('`\[[^\]]*\]`', '', $excerpt);
        $excerpt = preg_replace('/PUBLICIDAD/', '', $excerpt);

        return $excerpt;*/

    }
endif;

if (!function_exists('get_body_data')):
    function get_body_data($more_link_text = '(more...)', $stripteaser = 0, $more_file = '') {
        $content = get_the_content($more_link_text, $stripteaser, $more_file);
        $content = apply_filters('the_content', $content);
        $content = str_replace(']]>', ']]&gt;', $content);
        return $content;
    }
endif;

//Figure Source
if (!function_exists('extract_url')) :
    function extract_url($size)
    {
        global $post;
        $thumb = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), $size);
        $url = $thumb['0'];
        echo $url;
    }
endif;

//Get Slug
if (!function_exists('simple_the_slug')) :
    function simple_the_slug()
    {
        global $post;
        $post_slug = $post->post_name;

        echo "$post_slug";
    }
endif;


if (!function_exists('get_first_paragraph')) :
    function get_first_paragraph()
    {
        global $post;

        $str = wpautop(get_the_content());
        $str = strip_tags($str);
        $str = substr($str, 0, 420);
        $str = strip_tags($str, '<a><strong><em>');
        echo '' . $str . '';
    }
endif;

if (!function_exists('get_the_slug')):
    function get_the_slug($id = null)
    {
        if (empty($id)):
            global $post;
            if (empty($post))
                return ''; // No global $post var available.
            $id = $post->ID;
        endif;

        $slug = basename(get_permalink($id));
        return $slug;
    }
endif;


if (!function_exists('the_slug')):
    function the_slug($id = null)
    {
        echo apply_filters('the_slug', get_the_slug($id));
    }
endif;


//Category Arguments
if (!function_exists('category_name')) :
    function category_name()
    {
        $category = get_the_category();
        echo esc_html($category[0]->cat_name);
    }
endif;

if (!function_exists('category_link')) :
    function category_link()
    {
        echo get_the_category_link();
    }
endif;

if (!function_exists('get_the_category_link')) :
    function get_the_category_link()
    {

        $category = get_the_category();

        if (isset($category[0])) {
            return '<a href="' . esc_url(get_category_link($category[0]->term_id)) . '">' . esc_html($category[0]->cat_name) . '</a>';
            
        }
        return "";
    }
endif;

if (!function_exists('get_the_category_link_with_data')) :
    function get_the_category_link_with_data($term_id, $cat_name)
    {
        return '<a href="' . esc_url(get_category_link($term_id)) . '">' . esc_html($cat_name) . '</a>';
    }
endif;

if(! function_exists('get_category_slug')) :
    function get_category_slug(){
        $cats =  get_the_category();
        return isset($cats[0]) ? $cats[0]->slug : '';
    }
endif;

if(! function_exists('category_link_with_class')) :
    function category_link_with_class($class_name){
        $cats =  get_the_category();
        if(isset($cats[0])){
            $cat = $cats[0];
            echo '<a href="'.esc_url(get_category_link($cat->term_id )).'" class="'.$class_name.'">'. esc_html($cat->name).'</a>';
        }
    }
endif;



//Set the publishing date
if (!function_exists('published_time')) :
    function published_time()
    {
        $pub = the_date('F jS, Y', '', '', FALSE);
        echo $pub;
    }
endif;

//Published Time Link
if (!function_exists('published_time_links')) :
    function published_time_links()
    {
        $archive_year = get_the_time('Y');
        $archive_month = get_the_time('m');
        $archive_day = get_the_time('d');

        echo get_day_link($archive_year, $archive_month, $archive_day);
    }
endif;

//Ser the term slug
if (!function_exists('term_slug')) :
    function term_slug()
    {
        global $post;
        $name = single_term_title();
        $terms = get_the_terms($post->ID, $name);

        if ($terms && !wp_error($terms)) {
            foreach ($terms as $term) {
                echo $term->slug;
            }
        }
    }
endif;

//Post Thumbnail Captions
if (!function_exists('the_post_thumbnail_caption')) :
    function the_post_thumbnail_caption()
    {
        global $post;

        $thumbnail_id = get_post_thumbnail_id($post->ID);
        $thumbnail_image = get_posts(array('p' => $thumbnail_id, 'post_type' => 'attachment'));

        if ($thumbnail_image && isset($thumbnail_image[0])) {
            echo '<figcaption><span>' . $thumbnail_image[0]->post_title . '</span></figcaption>';
        }
    }
endif;

//Modify Read More Link
if (!function_exists('modify_read_more_link')) :
    function modify_read_more_link()
    {
        return ' <a class="btn-more" href="' . get_permalink() . '"> Leer art&iacute;culo completo...</a>';
    }
endif;

if (!function_exists('new_excerpt_more')) :
    function new_excerpt_more($more)
    {
        global $post;
        return ' <a class="btn-more" href="' . get_permalink($post->ID) . '"> Leer art&iacute;culo completo...</a>';
    }
endif;

//Time Filters
if (!function_exists('time_ago')) :
    function time_ago()
    {
        echo get_time_ago();
    }
endif;

if(!function_exists('get_time_ago')) {

    function get_time_ago()
    {
        global $post;
        $now = time();
        $date = strtotime($post->post_date);
        $sixMonthsAgo = 180 * 24 * 60 * 60;
        $human_time = 'hace ' . human_time_diff(get_the_time('U'), current_time('timestamp'));
        $mobile = wp_is_mobile();


        if (($now - $date) > $sixMonthsAgo && $mobile) {
            return get_the_time('j. M .Y');
        } elseif (($now - $date) > $sixMonthsAgo && !$mobile) {
            return get_the_time('j. F .Y');
        } else {
            return $human_time;
        }
    }

}

if (!function_exists('time_link')) :
    /**
     * Gets a nicely formatted string for the published date.
     */
    function time_link()
    {
        $time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
        if (get_the_time('U') !== get_the_modified_time('U')) {
            $time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
        }

        $time_string = sprintf($time_string,
            get_the_date(DATE_W3C),
            get_the_date(),
            get_the_modified_date(DATE_W3C),
            get_the_modified_date()
        );

        // Wrap the time string in a link, and preface it with 'Posted on'.
        return sprintf(
        /* translators: %s: post date */
            __('<span class="screen-reader-text">Posted on</span> %s', 'dreamstheme'),
            '<a href="' . esc_url(get_permalink()) . '" rel="bookmark">' . $time_string . '</a>'
        );
    }
endif;

/*  */


/* Related post by post connector */

if(!function_exists('get_related_post_by_post_conector')):
    function get_related_post_by_post_conector($post_id, $lenght = 2){
        $related_post = array();

            if(class_exists('Post_Connector')){ 
                $related_post = Post_Connector::API()->get_children('articulos-relacionados', $post_id);
                $related_post = array_slice($related_post, 0, $lenght);
            }

        return $related_post;
    }
endif;

/* Get First Porst */

if(!function_exists('get_first_post')):
  
    function get_first_post() {

		$stiky_posts_id = get_option('sticky_posts');
		if(count($stiky_posts_id) == 0) return NULL;

		$args = array(

			'no_found_rows' => true,
			'update_post_meta_cache' => false,
			'update_post_term_cache' => false,

			'posts_per_page' => 1,
			'post_type' => 'any',
			'post__in' => $stiky_posts_id,

		);

		$query = new WP_Query($args);
		$count = 0;

		while ($query->have_posts()) :
			$query->the_post();

			$count++;

			global $post, $prefix;
			$first_post = new stdClass();

			$first_post->ID = $post->ID;
			$first_post->post_title = $post->post_title;


			$first_post->layout_type = $post->den_layouts;
			$first_post->layout_url = $post->den_layout_url;

			$first_post->category_link = get_the_category_link();

			$first_post->post_permalink = get_the_permalink();

			$first_post->author_posts_link = get_the_author_posts_link();
			$first_post->time_ago = get_time_ago();

			$first_post->excerpt = excerpt(720);

			$first_post->post_thumbnail640 = get_the_post_thumbnail($post, 'dreams-640x360', array('class' => 'f4_module-image--full lazyload', 'data-object-fit' => 'cover'));
			$first_post->post_thumbnail_full = get_the_post_thumbnail($post, 'full', array('class' => 'f4_module-image--full lazyload', 'data-object-fit' => 'cover'));
			$first_post->post_thumbnail640_url = get_the_post_thumbnail_url($post, 'dreams-640x360');



			if($first_post->layout_type === 'front-5'){
				$video_url =  get_post_meta($first_post->ID, "{$prefix}layout_video_url", true);
				$image_logos_id = get_post_meta($first_post->ID, "{$prefix}layout_video_logo", true);
				$image_background_id = get_post_meta($first_post->ID, "{$prefix}layout_video_background", true);
				$layout_fixed_background = get_post_meta($first_post->ID, "{$prefix}layout_fixed_background", true);

				$first_post->video_url = $video_url;
				$first_post->image_logos = wp_get_attachment_url($image_logos_id);
				$first_post->image_background = wp_get_attachment_url($image_background_id);
				$first_post->layout_fixed_background = $layout_fixed_background;
			}


			if($count == 1) {
				wp_reset_postdata();
				return new WP_Post($first_post);
			}

		endwhile;

		wp_reset_postdata();




		return NULL;
	}

endif;

/* Tagging banner */

if (!function_exists('post_tagging_banner')):
  function post_tagging_banner() {
    global $prefix;
    $settings = get_option( $prefix . 'forbes40_settings' );
    $active = $prefix . 'tagging_activated';
    $tag = $prefix . 'tag_id';
    $active_tag = $settings[$tag];
    if(($settings[$active] != 0) && (!empty($active_tag))) {
  	  if (has_tag(array($active_tag))) :
        return " ribbon-$active_tag";
  	  endif;
    }
  }
endif;

if (!function_exists('tagging_banner')):
 function tagging_banner() {
  $tagging_banner = post_tagging_banner();
  echo $tagging_banner;
 }
endif;

if (!function_exists('get_dreams_ads')):


	function get_dreams_ads($type)
	{

		$i = '';
		$cat4 = get_the_category(get_query_var('cat'), false);


		if($cat4 && is_single() && is_singular()){
			$categories = get_the_category(get_query_var('cat'));
			$cat_name = $categories[0]->term_taxonomy_id;
		}


		if(is_home()) {
			$i .= "<div id=\"div-gpt-ad-f-home-$type\"></div>";

		} else if(is_category()) {

			$cat = get_category(get_query_var('cat'));
			$current_category = $cat->term_taxonomy_id;

			$i .= "<div id=\"div-gpt-ad-f-$current_category-$type\"></div>";


		} else if(is_tag()) {

			$tag         = get_queried_object();
			$current_tag = $tag->term_taxonomy_id;

			$i .= "<div id=\"div-gpt-ad-f-tag_$current_tag-$type\"></div>";

		} else if (isset($cat_name) && has_category($cat_name) && is_single()  && !is_singular('brand_voice')){

			global $post, $prefix;
			$meta_has_ad_chekbox = rwmb_meta( $prefix .'has_ad', 'type=checkbox');
			if(!empty($meta_has_ad_chekbox)) {
				$post_ID = $post->ID;
				$i .= "<div id=\"div-gpt-ad-f-$post_ID-s-$type\"></div>";

			} else {
				$i .= "<div id=\"div-gpt-ad-f-$cat_name-s-$type\"></div>";
			}

		} else if (is_singular('brand_voice')) {
			global $post;

			$terms = wp_get_post_terms( $post->ID, 'brand');

				if ( $terms != null ) {
					foreach($terms as $term) {
						$current_terms = $term->term_taxonomy_id;
						$i .= "<div id=\"div-gpt-ad-f-bv_$current_terms-s-$type\"></div>";
						//var_dump($current_terms);
					}
				}

		} else if(is_tax("brand")) {

			$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
			$i .= "<div id=\"div-gpt-ad-f-bv_$term->term_taxonomy_id-$type\"></div>";


		} else if (is_post_type_archive("forbes_life") || is_post_type_archive("brand_voice")) {

			$lbh = get_queried_object();
			$lbh_ID = $lbh->has_archive;

			$i .= "<div id=\"div-gpt-ad-f-$lbh_ID-$type\"></div>";


		} else if (is_page() || is_page_template()) {

			$get_page_info = get_queried_object();
			$page_id = $get_page_info->ID;
 			$i .= "<div id=\"div-gpt-ad-f-default-$type\"></div>";



		} else if (is_404()) {
 			$i .= "<div id=\"div-gpt-ad-f-default-$type\"></div>";



		} else {

			$i .= "<div id=\"div-gpt-ad-f-default-s-$type\"></div>";

		}


		if(!empty($i)){
			return $i;
		}


	}

endif;

if(!function_exists('the_dreams_ads')) :
  function the_dreams_ads($type){
  	$ads = get_dreams_ads($type);
  	if(!empty($ads)){
  	  echo $ads;
  	}
  }
endif;

//Add Avatar to MetaDisplay
if (!function_exists('get_the_avatar')):
    function get_the_avatar($userid)
    {
        global $prefix;
        $image_ids = get_user_meta($userid, $prefix . 'avatar', false); // Media fields are always multiple.
        $author_name = get_the_author_meta('display_name', $userid);
        $a = '';



        foreach ($image_ids as $image_id) {
            $image = RWMB_Image_Field::file_info($image_id, array('size' => 'full'));
            $a = "<img class=\"f4_module-image--full photo lazyload\" src=\"{$image['url']}\" alt=\"$author_name\" data-object-fit=\"cover\" width=\"80\" height=\"80\"  />";
        }

        if (isset($image_id)) {
            echo $a;
        } else {
            echo "<img class=\"f4_module-image--full photo lazyload\" src=\"http://cdn.forbes.com.mx/userphoto/$userid.thumbnail.jpg\" alt=\"$author_name\"  data-object-fit=\"cover\" width=\"80\" height=\"80\" />";
        }
    }
endif;