<?php
	/* Template Name: Counter */
?>

<html lang="en" class="">
	<head>
		<meta charset="UTF-8" />
		<link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
		<style>
			body,
			html {
				height: 100%;
                background-color: #EED961;
                align-items: center;
                display: flex;
                justify-content: center;
			}

			.counter_i {
				display: flex;
				align-items: center;
				justify-content: center;
				flex-flow: row wrap;
				height: 250px;
				margin: 0;
				background-color: #EED961;
			}

			.logo {
				display: flex;
				flex-direction: column;
				align-items: center;
				justify-content: center;
				height: 250px;
			}
			.logo img {
				width: 200px;
			}
			.social{
				text-align: center;
			}
			.social ul{
				list-style: none;
				display: flex;
				flex-direction: row;
				justify-content: center;
				padding: 0;
				margin: .5em 0 0;
			}
			
			.social ul li{
				margin: 0 .5em;
			}
			
			.social a{
				text-decoration: none;
				display: block;
				color: #1D252F;
			}
			
			.social img{
				height: 20px!important;
				margin: 0 1em;
				width: auto;
			}

		</style>
	</head>

	<body>
		<div class="counter_i">
			<div class="logo">
				<img data-src="https://cdn.forbes.co/2019/11/avisoCo.png" src="https://cdn.forbes.co/2019/11/avisoCo.png" alt="">
				<div class="social">
					<ul>
						<li>
							<a href="https://www.facebook.com/" target="_blank" rel="noopener noreferrer">
								<img data-src="https://cdn.forbes.co/2019/11/face.png" src="https://cdn.forbes.co/2019/11/face.png" alt="Facebook"/>
								<p>Terreno de Juego</p>
							</a>
						</li>
						<li>
							<a href="https://twitter.com/" target="_blank" rel="noopener noreferrer">
								<img data-src="https://cdn.forbes.co/2019/11/twit.png" src="https://cdn.forbes.co/2019/11/twit.png" alt="Twitter"/>
								<p>@terrenodejuego</p>
							</a>
						</li>
						<li>
							<a href="https://www.instagram.com/" target="_blank" rel="noopener noreferrer">
								<img data-src="https://cdn.forbes.co/2019/11/insta.png" src="https://cdn.forbes.co/2019/11/insta.png" alt="Instagram"/>
								<p>@terrenodejuego</p>
							</a>
						</li>
					</ul>
				</div>	
			</div>
			<div id="demo" style="font-size: 2rem; font-family: 'Roboto', sans-serif;"></div>
		</div>
        <script>
            var countDownDate = new Date("Nov 11, 2019 13:00:00").getTime();

            var x = setInterval(function() {

            var now = new Date().getTime();
                
            var distance = countDownDate - now;
                
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);
                
            document.getElementById("demo").innerHTML = days + " días " + hours + " horas "
            + minutes + " minutos " + seconds + " segundos ";
                
            if (distance < 0) {
                clearInterval(x);
                document.getElementById("demo").innerHTML = "¡Al fin!";
            }
            }, 1000);
        
        </script>
	</body>
</html>
