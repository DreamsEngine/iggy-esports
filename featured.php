<?php $pinno_feat_layout = get_option('pinno_feat_layout'); if( $pinno_feat_layout == "0" ) { ?>
	<div class="pinno-main-box">
		<section id="pinno-feat1-wrap" class="left relative">
			<div class="pinno-feat1-right-out left relative">
				<div class="pinno-feat1-right-in">
					<div class="pinno-feat1-main left relative">
						<div class="pinno-feat1-left-wrap relative">
							<?php global $do_not_duplicate; global $post; $recent = new WP_Query(array( 'tag' => get_option('pinno_feat_posts_tags'), 'posts_per_page' => '1', 'ignore_sticky_posts'=> 1 )); while($recent->have_posts()) : $recent->the_post(); $do_not_duplicate[] = $post->ID; ?>
								<a href="<?php the_permalink(); ?>" rel="bookmark">
								<div class="pinno-feat1-feat-wrap left relative">
									<div class="pinno-feat1-feat-img left relative">
										<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
											<?php the_post_thumbnail('pinno-port-thumb'); ?>
										<?php } ?>
										<?php if ( has_post_format( 'video' )) { ?>
											<div class="pinno-vid-box-wrap pinno-vid-marg">
												<i class="fa fa-2 fa-play" aria-hidden="true"></i>
											</div><!--pinno-vid-box-wrap-->
										<?php } else if ( has_post_format( 'gallery' )) { ?>
											<div class="pinno-vid-box-wrap">
												<i class="fa fa-2 fa-camera" aria-hidden="true"></i>
											</div><!--pinno-vid-box-wrap-->
										<?php } ?>
									</div><!--pinno-feat1-feat-img-->
									<div class="pinno-feat1-feat-text left relative">
										<div class="pinno-cat-date-wrap left relative">
											<span class="pinno-cd-cat left relative"><?php $category = get_the_category(); echo esc_html( $category[0]->cat_name ); ?></span><span class="pinno-cd-date left relative"><?php printf( esc_html__( '%s ago', 'iggy-type-0' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span>
										</div><!--pinno-cat-date-wrap-->
										<?php if(get_post_meta($post->ID, "pinno_featured_headline", true)): ?>
											<h2><?php echo esc_html(get_post_meta($post->ID, "pinno_featured_headline", true)); ?></h2>
										<?php else: ?>
											<h2 class="pinno-stand-title"><?php the_title(); ?></h2>
										<?php endif; ?>
										<p><?php echo wp_trim_words( get_the_excerpt(), 20, '...' ); ?></p>
									</div><!--pinno-feat1-feat-text-->
								</div><!--pinno-feat1-feat-wrap-->
								</a>
							<?php endwhile; wp_reset_postdata(); ?>
							<div class="pinno-feat1-sub-wrap left relative">
								<?php global $do_not_duplicate; global $post; $recent = new WP_Query(array( 'tag' => get_option('pinno_feat_posts_tags'), 'post__not_in'=>$do_not_duplicate, 'posts_per_page' => '2', 'ignore_sticky_posts'=> 1  )); while($recent->have_posts()) : $recent->the_post(); $do_not_duplicate[] = $post->ID; ?>
									<a href="<?php the_permalink(); ?>" rel="bookmark">
									<div class="pinno-feat1-sub-cont left relative">
										<div class="pinno-feat1-sub-img left relative">
											<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
												<?php the_post_thumbnail('pinno-large-thumb', array( 'class' => 'pinno-reg-img' )); ?>
												<?php the_post_thumbnail('pinno-mid-thumb', array( 'class' => 'pinno-mob-img' )); ?>
											<?php } ?>
											<?php if ( has_post_format( 'video' )) { ?>
												<div class="pinno-vid-box-wrap pinno-vid-marg">
													<i class="fa fa-2 fa-play" aria-hidden="true"></i>
												</div><!--pinno-vid-box-wrap-->
											<?php } else if ( has_post_format( 'gallery' )) { ?>
												<div class="pinno-vid-box-wrap">
													<i class="fa fa-2 fa-camera" aria-hidden="true"></i>
												</div><!--pinno-vid-box-wrap-->
											<?php } ?>
										</div><!--pinno-feat1-sub-img-->
										<div class="pinno-feat1-sub-text">
											<div class="pinno-cat-date-wrap left relative">
												<span class="pinno-cd-cat left relative"><?php $category = get_the_category(); echo esc_html( $category[0]->cat_name ); ?></span><span class="pinno-cd-date left relative"><?php printf( esc_html__( '%s ago', 'iggy-type-0' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span>
											</div><!--pinno-cat-date-wrap-->
											<h2><?php the_title(); ?></h2>
										</div><!--pinno-feat1-sub-text-->
									</div><!--pinno-feat1-sub-cont-->
									</a>
								<?php endwhile; wp_reset_postdata(); ?>
							</div><!--pinno-feat1-sub-wrap-->
						</div><!--pinno-feat1-left-wrap-->
						<div class="pinno-feat1-mid-wrap left relative">
							<h3 class="pinno-feat1-pop-head"><span class="pinno-feat1-pop-head"><?php echo esc_html(get_option('pinno_pop_head')); ?></span></h3>
							<div class="pinno-feat1-pop-wrap left relative">
								<?php global $do_not_duplicate; global $post; $pop_days = esc_html(get_option('pinno_pop_days')); $popular_days_ago = "$pop_days days ago"; $recent = new WP_Query(array('posts_per_page' => '5', 'ignore_sticky_posts'=> 1, 'post__not_in' => $do_not_duplicate, 'orderby' => 'meta_value_num', 'order' => 'DESC', 'meta_key' => 'post_views_count', 'date_query' => array( array( 'after' => $popular_days_ago )) )); while($recent->have_posts()) : $recent->the_post(); ?>
									<a href="<?php the_permalink(); ?>" rel="bookmark">
									<div class="pinno-feat1-pop-cont left relative">
										<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
											<div class="pinno-feat1-pop-img left relative">
												<?php the_post_thumbnail('pinno-mid-thumb', array( 'class' => 'pinno-reg-img' )); ?>
												<?php the_post_thumbnail('pinno-small-thumb', array( 'class' => 'pinno-mob-img' )); ?>
												<?php if ( has_post_format( 'video' )) { ?>
													<div class="pinno-vid-box-wrap pinno-vid-box-mid pinno-vid-marg">
														<i class="fa fa-2 fa-play" aria-hidden="true"></i>
													</div><!--pinno-vid-box-wrap-->
												<?php } else if ( has_post_format( 'gallery' )) { ?>
													<div class="pinno-vid-box-wrap pinno-vid-box-mid">
														<i class="fa fa-2 fa-camera" aria-hidden="true"></i>
													</div><!--pinno-vid-box-wrap-->
												<?php } ?>
											</div><!--pinno-feat1-pop-img-->
										<?php } ?>
										<div class="pinno-feat1-pop-text left relative">
											<div class="pinno-cat-date-wrap left relative">
												<span class="pinno-cd-cat left relative"><?php $category = get_the_category(); echo esc_html( $category[0]->cat_name ); ?></span><span class="pinno-cd-date left relative"><?php printf( esc_html__( '%s ago', 'iggy-type-0' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span>
											</div><!--pinno-cat-date-wrap-->
											<h2><?php the_title(); ?></h2>
										</div><!--pinno-feat1-pop-text-->
									</div><!--pinno-feat1-pop-cont-->
									</a>
								<?php endwhile; wp_reset_postdata(); ?>
							</div><!--pinno-feat1-pop-wrap-->
						</div><!--pinno-feat1-mid-wrap-->
					</div><!--pinno-feat1-main-->
				</div><!--pinno-feat1-right-in-->
				<div class="pinno-feat1-right-wrap left relative">
					<?php if(get_option('pinno_feat_ad')) { ?>
						<div class="pinno-feat1-list-ad left relative">
							<span class="pinno-ad-label"><?php esc_html_e( 'Advertisement', 'iggy-type-0' ); ?></span>
							<?php $pinno_feat_ad = get_option('pinno_feat_ad'); if ($pinno_feat_ad) { echo html_entity_decode($pinno_feat_ad); } ?>
						</div><!--pinno-feat1-list-ad-->
					<?php } ?>
					<div class="pinno-feat1-list-wrap left relative">
						<div class="pinno-feat1-list-head-wrap left relative">
							<ul class="pinno-feat1-list-buts left relative">
								<li class="pinno-feat-col-tab"><a href="#pinno-feat-tab-col1"><span class="pinno-feat1-list-but"><?php esc_html_e( 'Latest', 'iggy-type-0' ); ?></span></a></li>
								<?php query_posts(array( 'tax_query' => array( array( 'taxonomy' => 'post_format', 'field' => 'slug', 'terms' => 'post-format-video' )) )); if (have_posts()) : ?>
									<li><a href="#pinno-feat-tab-col2"><span class="pinno-feat1-list-but"><?php esc_html_e( 'Videos', 'iggy-type-0' ); ?></span></a></li>
								<?php endif; wp_reset_query(); ?>
								<?php query_posts(array( 'tax_query' => array( array( 'taxonomy' => 'post_format', 'field' => 'slug', 'terms' => 'post-format-gallery' )) )); if (have_posts()) : ?>
									<li><a href="#pinno-feat-tab-col3"><span class="pinno-feat1-list-but"><?php esc_html_e( 'Galleries', 'iggy-type-0' ); ?></span></a></li>
								<?php endif; wp_reset_query(); ?>
							</ul>
						</div><!--pinno-feat1-list-head-wrap-->
						<div id="pinno-feat-tab-col1" class="pinno-feat1-list left relative pinno-tab-col-cont">
							<?php global $do_not_duplicate; if (isset($do_not_duplicate)) { if(get_option('pinno_feat_ad')) { $pinno_feat_list_num = 10; } else { $pinno_feat_list_num = 13; }; $paged = (get_query_var('page')) ? get_query_var('page') : 1; query_posts(array( 'posts_per_page' => $pinno_feat_list_num, 'post__not_in'=>$do_not_duplicate, 'paged' =>$paged, 'ignore_sticky_posts'=> 1 )); if (have_posts()) : while (have_posts()) : the_post(); ?>
								<a href="<?php the_permalink(); ?>" rel="bookmark">
								<div class="pinno-feat1-list-cont left relative">
									<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
										<div class="pinno-feat1-list-out relative">
											<div class="pinno-feat1-list-img left relative">
												<?php the_post_thumbnail('pinno-small-thumb'); ?>
											</div><!--pinno-feat1-list-img-->
											<div class="pinno-feat1-list-in">
												<div class="pinno-feat1-list-text">
													<div class="pinno-cat-date-wrap left relative">
														<span class="pinno-cd-cat left relative"><?php $category = get_the_category(); echo esc_html( $category[0]->cat_name ); ?></span><span class="pinno-cd-date left relative"><?php printf( esc_html__( '%s ago', 'iggy-type-0' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span>
													</div><!--pinno-cat-date-wrap-->
													<h2><?php the_title(); ?></h2>
												</div><!--pinno-feat1-list-text-->
											</div><!--pinno-feat1-list-in-->
										</div><!--pinno-feat1-list-out-->
									<?php } else { ?>
										<div class="pinno-feat1-list-text">
											<div class="pinno-cat-date-wrap left relative">
												<span class="pinno-cd-cat left relative"><?php $category = get_the_category(); echo esc_html( $category[0]->cat_name ); ?></span><span class="pinno-cd-date left relative"><?php printf( esc_html__( '%s ago', 'iggy-type-0' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span>
											</div><!--pinno-cat-date-wrap-->
											<h2><?php the_title(); ?></h2>
										</div><!--pinno-feat1-list-text-->
									<?php } ?>
								</div><!--pinno-feat1-list-cont-->
								</a>
							<?php endwhile; endif; wp_reset_query(); } ?>
						</div><!--pinno-feat-tab-col1-->
						<div id="pinno-feat-tab-col2" class="pinno-feat1-list left relative pinno-tab-col-cont">
							<?php global $do_not_duplicate; if (isset($do_not_duplicate)) { if(get_option('pinno_feat_ad')) { $pinno_feat_list_num = 10; } else { $pinno_feat_list_num = 13; }; $paged = (get_query_var('page')) ? get_query_var('page') : 1; query_posts(array( 'tax_query' => array( array( 'taxonomy' => 'post_format', 'field' => 'slug', 'terms' => 'post-format-video' )), 'posts_per_page' => $pinno_feat_list_num, 'ignore_sticky_posts'=> 1, 'post__not_in'=>$do_not_duplicate, 'paged' =>$paged )); if (have_posts()) : while (have_posts()) : the_post(); ?>
								<a href="<?php the_permalink(); ?>" rel="bookmark">
								<div class="pinno-feat1-list-cont left relative">
									<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
										<div class="pinno-feat1-list-out relative">
											<div class="pinno-feat1-list-img left relative">
												<?php the_post_thumbnail('pinno-small-thumb'); ?>
											</div><!--pinno-feat1-list-img-->
											<div class="pinno-feat1-list-in">
												<div class="pinno-feat1-list-text">
													<div class="pinno-cat-date-wrap left relative">
														<span class="pinno-cd-cat left relative"><?php $category = get_the_category(); echo esc_html( $category[0]->cat_name ); ?></span><span class="pinno-cd-date left relative"><?php printf( esc_html__( '%s ago', 'iggy-type-0' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span>
													</div><!--pinno-cat-date-wrap-->
													<h2><?php the_title(); ?></h2>
												</div><!--pinno-feat1-list-text-->
											</div><!--pinno-feat1-list-in-->
										</div><!--pinno-feat1-list-out-->
									<?php } else { ?>
										<div class="pinno-feat1-list-text">
											<div class="pinno-cat-date-wrap left relative">
												<span class="pinno-cd-cat left relative"><?php $category = get_the_category(); echo esc_html( $category[0]->cat_name ); ?></span><span class="pinno-cd-date left relative"><?php printf( esc_html__( '%s ago', 'iggy-type-0' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span>
											</div><!--pinno-cat-date-wrap-->
											<h2><?php the_title(); ?></h2>
										</div><!--pinno-feat1-list-text-->
									<?php } ?>
								</div><!--pinno-feat1-list-cont-->
								</a>
							<?php endwhile; endif; wp_reset_query(); } ?>
						</div><!--pinno-feat-tab-col2-->
						<div id="pinno-feat-tab-col3" class="pinno-feat1-list left relative pinno-tab-col-cont">
							<?php global $do_not_duplicate; if (isset($do_not_duplicate)) { if(get_option('pinno_feat_ad')) { $pinno_feat_list_num = 10; } else { $pinno_feat_list_num = 13; }; $paged = (get_query_var('page')) ? get_query_var('page') : 1; query_posts(array( 'tax_query' => array( array( 'taxonomy' => 'post_format', 'field' => 'slug', 'terms' => 'post-format-gallery' )), 'posts_per_page' => $pinno_feat_list_num, 'ignore_sticky_posts'=> 1, 'post__not_in'=>$do_not_duplicate, 'paged' =>$paged )); if (have_posts()) : while (have_posts()) : the_post(); ?>
								<a href="<?php the_permalink(); ?>" rel="bookmark">
								<div class="pinno-feat1-list-cont left relative">
									<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
										<div class="pinno-feat1-list-out relative">
											<div class="pinno-feat1-list-img left relative">
												<?php the_post_thumbnail('pinno-small-thumb'); ?>
											</div><!--pinno-feat1-list-img-->
											<div class="pinno-feat1-list-in">
												<div class="pinno-feat1-list-text">
													<div class="pinno-cat-date-wrap left relative">
														<span class="pinno-cd-cat left relative"><?php $category = get_the_category(); echo esc_html( $category[0]->cat_name ); ?></span><span class="pinno-cd-date left relative"><?php printf( esc_html__( '%s ago', 'iggy-type-0' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span>
													</div><!--pinno-cat-date-wrap-->
													<h2><?php the_title(); ?></h2>
												</div><!--pinno-feat1-list-text-->
											</div><!--pinno-feat1-list-in-->
										</div><!--pinno-feat1-list-out-->
									<?php } else { ?>
										<div class="pinno-feat1-list-text">
											<div class="pinno-cat-date-wrap left relative">
												<span class="pinno-cd-cat left relative"><?php $category = get_the_category(); echo esc_html( $category[0]->cat_name ); ?></span><span class="pinno-cd-date left relative"><?php printf( esc_html__( '%s ago', 'iggy-type-0' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span>
											</div><!--pinno-cat-date-wrap-->
											<h2><?php the_title(); ?></h2>
										</div><!--pinno-feat1-list-text-->
									<?php } ?>
								</div><!--pinno-feat1-list-cont-->
								</a>
							<?php endwhile; endif; wp_reset_query(); } ?>
						</div><!--pinno-feat-tab-col3-->
					</div><!--pinno-feat1-list-wrap-->
				</div><!--pinno-feat1-right-wrap-->
			</div><!--pinno-feat1-right-out-->
		</section><!--pinno-feat1-wrap-->
	</div><!--pinno-main-box-->
<?php } else if( $pinno_feat_layout == "1" ) { ?>
	<section id="pinno-feat2-wrap" class="left relative">
		<div class="pinno-feat2-top left relative">
			<?php global $do_not_duplicate; global $post; $recent = new WP_Query(array( 'tag' => get_option('pinno_feat_posts_tags'), 'posts_per_page' => '1', 'ignore_sticky_posts'=> 1  )); while($recent->have_posts()) : $recent->the_post(); $do_not_duplicate[] = $post->ID; ?>
				<a href="<?php the_permalink(); ?>" rel="bookmark">
				<div class="pinno-feat2-top-story left relative">
					<div class="pinno-feat2-top-img left relative">
						<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
							<?php the_post_thumbnail('', array( 'class' => 'pinno-reg-img' )); ?>
							<?php the_post_thumbnail('pinno-port-thumb', array( 'class' => 'pinno-mob-img' )); ?>
						<?php } ?>
					</div><!--pinno-feat2-top-img-->
					<div class="pinno-feat2-top-text-wrap hola">
						<div class="pinno-feat2-top-text-box">
							<div class="pinno-feat2-top-text left relative">

								<?php if(get_post_meta($post->ID, "pinno_featured_headline", true)): ?>
									<h2><?php echo esc_html(get_post_meta($post->ID, "pinno_featured_headline", true)); ?></h2>
								<?php else: ?>
									<a href="<?php $category_arr = get_the_category($post->ID); echo get_category_link($category_arr[0]); ?>"><strong><?php echo $category_arr[0]->cat_name; ?></strong></a>
									<a href="<?php echo the_permalink(); ?>"><h2 class="pinno-stand-title"><?php the_title(); ?></h2>
								<?php endif; ?>
								<p><?php echo wp_trim_words( get_the_excerpt(), 20, '...' ); ?></p>
							</div><!--pinno-feat2-top-text-->
						</div><!--pinno-feat2-top-text-box-->
					</div><!--pinno-feat2-top-text-wrap-->
				</div><!--pinno-feat2-top-story-->
				</a>
			<?php endwhile; wp_reset_postdata(); ?>
		</div><!--pinno-feat2-top-->
		<div class="pinno-feat2-bot-wrap left relative">
			<div class="pinno-main-box">
				<div class="pinno-feat2-bot left relative">
					<?php global $do_not_duplicate; global $post; $recent = new WP_Query(array( 'tag' => get_option('pinno_feat_posts_tags'), 'post__not_in'=>$do_not_duplicate, 'posts_per_page' => '4', 'ignore_sticky_posts'=> 1  )); while($recent->have_posts()) : $recent->the_post(); $do_not_duplicate[] = $post->ID; ?>
						<a href="<?php the_permalink(); ?>" rel="bookmark">
						<div class="pinno-feat2-bot-story left relative">
							<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
								<div class="pinno-feat2-bot-img left relative">
									<?php the_post_thumbnail('pinno-mid-thumb', array( 'class' => 'pinno-reg-img' )); ?>
									<?php the_post_thumbnail('pinno-small-thumb', array( 'class' => 'pinno-mob-img' )); ?>
									<?php if ( has_post_format( 'video' )) { ?>
										<div class="pinno-vid-box-wrap pinno-vid-box-mid pinno-vid-marg">
											<i class="fa fa-2 fa-play" aria-hidden="true"></i>
										</div><!--pinno-vid-box-wrap-->
									<?php } else if ( has_post_format( 'gallery' )) { ?>
										<div class="pinno-vid-box-wrap pinno-vid-box-mid">
											<i class="fa fa-2 fa-camera" aria-hidden="true"></i>
										</div><!--pinno-vid-box-wrap-->
									<?php } ?>
								</div><!--pinno-feat2-bot-img-->
							<?php } ?>
							<div class="pinno-feat2-bot-text left relative">
								<div class="pinno-cat-date-wrap left relative">
									<span class="pinno-cd-cat left relative"><?php $category = get_the_category(); echo esc_html( $category[0]->cat_name ); ?></span><span class="pinno-cd-date left relative"><?php printf( esc_html__( '%s ago', 'iggy-type-0' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span>
								</div><!--pinno-cat-date-wrap-->
								<h2><?php the_title(); ?></h2>
							</div><!--pinno-feat2-bot-text-->
						</div><!--pinno-feat2-bot-story-->
						</a>
					<?php endwhile; wp_reset_postdata(); ?>
				</div><!--pinno-feat2-bot-->
			</div><!--pinno-main-box-->
		</div><!--pinno-feat2-bot-wrap-->
	</section><!--pinno-feat2-wrap-->
<?php } else if( $pinno_feat_layout == "2" ) { ?>
	<section id="pinno-feat3-wrap" class="left relative">
		<div class="pinno-main-box">
			<div class="pinno-feat3-cont">
				<div class="pinno-feat3-main-wrap left relative">
					<?php global $do_not_duplicate; global $post; $recent = new WP_Query(array( 'tag' => get_option('pinno_feat_posts_tags'), 'posts_per_page' => '1', 'ignore_sticky_posts'=> 1 )); while($recent->have_posts()) : $recent->the_post(); $do_not_duplicate[] = $post->ID; ?>
						<a href="<?php the_permalink(); ?>" rel="bookmark">
						<div class="pinno-feat3-main-story left relative">
							<div class="pinno-feat3-main-img left relative">
								<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
									<?php the_post_thumbnail('pinno-port-thumb'); ?>
								<?php } ?>
								<?php if ( has_post_format( 'video' )) { ?>
									<div class="pinno-vid-box-wrap pinno-vid-marg">
										<i class="fa fa-2 fa-play" aria-hidden="true"></i>
									</div><!--pinno-vid-box-wrap-->
								<?php } else if ( has_post_format( 'gallery' )) { ?>
									<div class="pinno-vid-box-wrap">
										<i class="fa fa-2 fa-camera" aria-hidden="true"></i>
									</div><!--pinno-vid-box-wrap-->
								<?php } ?>
							</div><!--pinno-feat3-main-img-->
							<div class="pinno-feat3-main-text">
								<div class="pinno-cat-date-wrap left relative">
									<span class="pinno-cd-cat left relative"><?php $category = get_the_category(); echo esc_html( $category[0]->cat_name ); ?></span><span class="pinno-cd-date left relative"><?php printf( esc_html__( '%s ago', 'iggy-type-0' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span>
								</div><!--pinno-cat-date-wrap-->
								<?php if(get_post_meta($post->ID, "pinno_featured_headline", true)): ?>
									<h2><?php echo esc_html(get_post_meta($post->ID, "pinno_featured_headline", true)); ?></h2>
								<?php else: ?>
									<h2 class="pinno-stand-title"><?php the_title(); ?></h2>
								<?php endif; ?>
								<p><?php echo wp_trim_words( get_the_excerpt(), 20, '...' ); ?></p>
							</div><!--pinno-feat3-main-text-->
						</div><!--pinno-feat3-main-story -->
						</a>
					<?php endwhile; wp_reset_postdata(); ?>
				</div><!--pinno-feat3-main-wrap-->
				<div class="pinno-feat3-sub-wrap left relative">
					<?php global $do_not_duplicate; global $post; $recent = new WP_Query(array( 'tag' => get_option('pinno_feat_posts_tags'), 'post__not_in'=>$do_not_duplicate, 'posts_per_page' => '2', 'ignore_sticky_posts'=> 1 )); while($recent->have_posts()) : $recent->the_post(); $do_not_duplicate[] = $post->ID; ?>
						<a href="<?php the_permalink(); ?>" rel="bookmark">
						<div class="pinno-feat3-sub-story left relative">
							<div class="pinno-feat3-sub-img left relative">
								<?php the_post_thumbnail('pinno-post-thumb', array( 'class' => 'pinno-reg-img' )); ?>
								<?php the_post_thumbnail('pinno-mid-thumb', array( 'class' => 'pinno-mob-img' )); ?>
								<?php if ( has_post_format( 'video' )) { ?>
									<div class="pinno-vid-box-wrap pinno-vid-marg">
										<i class="fa fa-2 fa-play" aria-hidden="true"></i>
									</div><!--pinno-vid-box-wrap-->
								<?php } else if ( has_post_format( 'gallery' )) { ?>
									<div class="pinno-vid-box-wrap">
										<i class="fa fa-2 fa-camera" aria-hidden="true"></i>
									</div><!--pinno-vid-box-wrap-->
								<?php } ?>
							</div><!--pinno-feat3-sub-img-->
							<div class="pinno-feat3-sub-text">
								<div class="pinno-cat-date-wrap left relative">
									<span class="pinno-cd-cat left relative"><?php $category = get_the_category(); echo esc_html( $category[0]->cat_name ); ?></span><span class="pinno-cd-date left relative"><?php printf( esc_html__( '%s ago', 'iggy-type-0' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span>
								</div><!--pinno-cat-date-wrap-->
								<h2><?php the_title(); ?></h2>
							</div><!--pinno-feat3-sub-text-->
						</div><!--pinno-feat3-sub-story-->
						</a>
					<?php endwhile; wp_reset_postdata(); ?>
				</div><!--pinno-feat3-sub-wrap-->
			</div><!--pinno-feat3-cont-->
		</div><!--pinno-main-box-->
	</section><!--pinno-feat3-wrap-->
<?php } else if( $pinno_feat_layout == "3" ) { ?>
	<section id="pinno-feat4-wrap" class="left relative">
		<div class="pinno-main-box">
			<div class="pinno-feat4-main left relative">
				<?php global $do_not_duplicate; global $post; $recent = new WP_Query(array( 'tag' => get_option('pinno_feat_posts_tags'), 'posts_per_page' => '1', 'ignore_sticky_posts'=> 1 )); while($recent->have_posts()) : $recent->the_post(); $do_not_duplicate[] = $post->ID; ?>
					<a href="<?php the_permalink(); ?>" rel="bookmark">
					<div class="pinno-feat4-main-story left relative">
						<div class="pinno-feat4-main-img left relative">
							<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
								<?php the_post_thumbnail('pinno-post-thumb', array( 'class' => 'pinno-reg-img' )); ?>
								<?php the_post_thumbnail('pinno-mid-thumb', array( 'class' => 'pinno-mob-img' )); ?>
							<?php } ?>
							<?php if ( has_post_format( 'video' )) { ?>
								<div class="pinno-vid-box-wrap pinno-vid-marg">
									<i class="fa fa-2 fa-play" aria-hidden="true"></i>
								</div><!--pinno-vid-box-wrap-->
							<?php } else if ( has_post_format( 'gallery' )) { ?>
								<div class="pinno-vid-box-wrap">
									<i class="fa fa-2 fa-camera" aria-hidden="true"></i>
								</div><!--pinno-vid-box-wrap-->
							<?php } ?>
						</div><!--pinno-feat4-main-img-->
						<div class="pinno-feat4-main-text left relative">
							<div class="pinno-cat-date-wrap left relative">
								<span class="pinno-cd-cat left relative"><?php $category = get_the_category(); echo esc_html( $category[0]->cat_name ); ?></span><span class="pinno-cd-date left relative"><?php printf( esc_html__( '%s ago', 'iggy-type-0' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span>
							</div><!--pinno-cat-date-wrap-->
							<h2><?php the_title(); ?></h2>
							<p><?php echo wp_trim_words( get_the_excerpt(), 20, '...' ); ?></p>
						</div><!--pinno-feat4-main-text-->
					</div><!--pinno-feat4-main-story-->
					</a>
				<?php endwhile; wp_reset_postdata(); ?>
			</div><!--pinno-feat4-main-->
		</div><!--pinno-main-box-->
	</section><!--pinno-feat4-wrap-->
<?php } else if( $pinno_feat_layout == "4" ) { ?>
	<div class="pinno-main-box">
		<section id="pinno-feat5-wrap" class="left relative">
			<div class="pinno-feat5-side-out left relative">
				<div class="pinno-feat5-side-in">
					<div class="pinno-feat5-main-wrap left relative">
						<div class="pinno-feat5-mid-wrap right relative">
							<?php global $do_not_duplicate; global $post; $recent = new WP_Query(array( 'tag' => get_option('pinno_feat_posts_tags'), 'posts_per_page' => '1', 'ignore_sticky_posts'=> 1 )); while($recent->have_posts()) : $recent->the_post(); $do_not_duplicate[] = $post->ID; ?>
								<a href="<?php the_permalink(); ?>" rel="bookmark">
								<div class="pinno-feat5-mid-main left relative">
									<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
										<div class="pinno-feat5-mid-main-img left relative">
											<?php the_post_thumbnail('pinno-large-thumb', array( 'class' => 'pinno-reg-img' )); ?>
											<?php the_post_thumbnail('pinno-port-thumb', array( 'class' => 'pinno-mob-img' )); ?>
											<?php if ( has_post_format( 'video' )) { ?>
												<div class="pinno-vid-box-wrap pinno-vid-marg">
													<i class="fa fa-2 fa-play" aria-hidden="true"></i>
												</div><!--pinno-vid-box-wrap-->
											<?php } else if ( has_post_format( 'gallery' )) { ?>
												<div class="pinno-vid-box-wrap">
													<i class="fa fa-2 fa-camera" aria-hidden="true"></i>
												</div><!--pinno-vid-box-wrap-->
											<?php } ?>
										</div><!--pinno-feat5-mid-main-img-->
									<?php } ?>
									<div class="pinno-feat5-mid-main-text left relative">
										<div class="pinno-cat-date-wrap left relative">
											<span class="pinno-cd-cat left relative"><?php $category = get_the_category(); echo esc_html( $category[0]->cat_name ); ?></span><span class="pinno-cd-date left relative"><?php printf( esc_html__( '%s ago', 'iggy-type-0' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span>
										</div><!--pinno-cat-date-wrap-->
										<h2><?php the_title(); ?></h2>
										<p><?php echo wp_trim_words( get_the_excerpt(), 20, '...' ); ?></p>
									</div><!--pinno-feat5-mid-main-text-->
								</div><!--pinno-feat5-mid-main-->
								</a>
							<?php endwhile; wp_reset_postdata(); ?>
							<div class="pinno-feat5-mid-sub-wrap left relative">
								<?php global $do_not_duplicate; global $post; $recent = new WP_Query(array( 'tag' => get_option('pinno_feat_posts_tags'), 'post__not_in'=>$do_not_duplicate, 'posts_per_page' => '3', 'ignore_sticky_posts'=> 1  )); while($recent->have_posts()) : $recent->the_post(); $do_not_duplicate[] = $post->ID; ?>
									<a href="<?php the_permalink(); ?>" rel="bookmark">
									<div class="pinno-feat5-mid-sub-story left relative">
										<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
											<div class="pinno-feat5-mid-sub-out right relative">
												<div class="pinno-feat5-mid-sub-img left relative">
													<?php the_post_thumbnail('pinno-mid-thumb', array( 'class' => 'pinno-reg-img' )); ?>
													<?php the_post_thumbnail('pinno-small-thumb', array( 'class' => 'pinno-mob-img' )); ?>
													<?php if ( has_post_format( 'video' )) { ?>
														<div class="pinno-vid-box-wrap pinno-vid-box-mid pinno-vid-marg">
															<i class="fa fa-2 fa-play" aria-hidden="true"></i>
														</div><!--pinno-vid-box-wrap-->
													<?php } else if ( has_post_format( 'gallery' )) { ?>
														<div class="pinno-vid-box-wrap pinno-vid-box-mid">
															<i class="fa fa-2 fa-camera" aria-hidden="true"></i>
														</div><!--pinno-vid-box-wrap-->
													<?php } ?>
												</div><!--pinno-feat5-mid-sub-img-->
												<div class="pinno-feat5-mid-sub-in">
													<div class="pinno-feat5-mid-sub-text left relative">
														<div class="pinno-cat-date-wrap left relative">
															<span class="pinno-cd-cat left relative"><?php $category = get_the_category(); echo esc_html( $category[0]->cat_name ); ?></span><span class="pinno-cd-date left relative"><?php printf( esc_html__( '%s ago', 'iggy-type-0' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span>
														</div><!--pinno-cat-date-wrap-->
														<h2><?php the_title(); ?></h2>
													</div><!--pinno-feat5-mid-sub-text-->
												</div><!--pinno-feat5-mid-sub-in-->
											</div><!--pinno-feat5-mid-sub-out-->
										<?php } else { ?>
											<div class="pinno-feat5-mis-sub-text left relative">
												<div class="pinno-cat-date-wrap left relative">
													<span class="pinno-cd-cat left relative"><?php $category = get_the_category(); echo esc_html( $category[0]->cat_name ); ?></span><span class="pinno-cd-date left relative"><?php printf( esc_html__( '%s ago', 'iggy-type-0' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span>
												</div><!--pinno-cat-date-wrap-->
												<h2><?php the_title(); ?></h2>
											</div><!--pinno-feat5-mis-sub-text-->
										<?php } ?>
									</div><!--pinno-feat5-mid-sub-story-->
									</a>
								<?php endwhile; wp_reset_postdata(); ?>
							</div><!--pinno-feat5-mid-sub-wrap-->
						</div><!--pinno-feat5-mid-wrap-->
						<div class="pinno-feat5-small-wrap left relative">
							<h3 class="pinno-feat1-pop-head"><span class="pinno-feat1-pop-head"><?php esc_html_e( 'Latest', 'iggy-type-0' ); ?></span></h3>
							<?php global $do_not_duplicate; if (isset($do_not_duplicate)) { $paged = (get_query_var('page')) ? get_query_var('page') : 1; query_posts(array( 'posts_per_page' => '1', 'post__not_in'=>$do_not_duplicate, 'paged' =>$paged, 'ignore_sticky_posts'=> 1 )); if (have_posts()) : while (have_posts()) : the_post(); $do_not_duplicate[] = $post->ID; ?>
								<a href="<?php the_permalink(); ?>" rel="bookmark">
								<div class="pinno-feat5-small-main left relative">
									<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
										<div class="pinno-feat5-small-main-img left relative">
											<?php the_post_thumbnail('pinno-mid-thumb'); ?>
											<?php if ( has_post_format( 'video' )) { ?>
												<div class="pinno-vid-box-wrap pinno-vid-marg">
													<i class="fa fa-2 fa-play" aria-hidden="true"></i>
												</div><!--pinno-vid-box-wrap-->
											<?php } else if ( has_post_format( 'gallery' )) { ?>
												<div class="pinno-vid-box-wrap">
													<i class="fa fa-2 fa-camera" aria-hidden="true"></i>
												</div><!--pinno-vid-box-wrap-->
											<?php } ?>
										</div><!--pinno-feat5-small-main-img-->
									<?php } ?>
									<div class="pinno-feat5-small-main-text left relative">
										<div class="pinno-cat-date-wrap left relative">
											<span class="pinno-cd-cat left relative"><?php $category = get_the_category(); echo esc_html( $category[0]->cat_name ); ?></span><span class="pinno-cd-date left relative"><?php printf( esc_html__( '%s ago', 'iggy-type-0' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span>
										</div><!--pinno-cat-date-wrap-->
										<h2><?php the_title(); ?></h2>
										<p><?php echo wp_trim_words( get_the_excerpt(), 20, '...' ); ?></p>
									</div><!--pinno-feat5-small-main-text-->
								</div><!--pinno-feat5-small-main-->
								</a>
							<?php endwhile; endif; wp_reset_query(); } ?>
							<div class="pinno-feat5-small-sub left relative">
								<?php global $do_not_duplicate; if (isset($do_not_duplicate)) { $paged = (get_query_var('page')) ? get_query_var('page') : 1; query_posts(array( 'posts_per_page' => '6', 'post__not_in'=>$do_not_duplicate, 'paged' =>$paged, 'ignore_sticky_posts'=> 1 )); if (have_posts()) : while (have_posts()) : the_post(); ?>
									<a href="<?php the_permalink(); ?>" rel="bookmark">
									<div class="pinno-feat1-list-cont left relative">
										<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
											<div class="pinno-feat1-list-out relative">
												<div class="pinno-feat1-list-img left relative">
													<?php the_post_thumbnail('pinno-small-thumb'); ?>
												</div><!--pinno-feat1-list-img-->
												<div class="pinno-feat1-list-in">
													<div class="pinno-feat1-list-text">
														<div class="pinno-cat-date-wrap left relative">
															<span class="pinno-cd-cat left relative"><?php $category = get_the_category(); echo esc_html( $category[0]->cat_name ); ?></span><span class="pinno-cd-date left relative"><?php printf( esc_html__( '%s ago', 'iggy-type-0' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span>
														</div><!--pinno-cat-date-wrap-->
														<h2><?php the_title(); ?></h2>
													</div><!--pinno-feat1-list-text-->
												</div><!--pinno-feat1-list-in-->
											</div><!--pinno-feat1-list-out-->
										<?php } else { ?>
											<div class="pinno-feat1-list-text">
												<div class="pinno-cat-date-wrap left relative">
													<span class="pinno-cd-cat left relative"><?php $category = get_the_category(); echo esc_html( $category[0]->cat_name ); ?></span><span class="pinno-cd-date left relative"><?php printf( esc_html__( '%s ago', 'iggy-type-0' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span>
												</div><!--pinno-cat-date-wrap-->
												<h2><?php the_title(); ?></h2>
											</div><!--pinno-feat1-list-text-->
										<?php } ?>
									</div><!--pinno-feat1-list-cont-->
									</a>
								<?php endwhile; endif; wp_reset_query(); } ?>
							</div><!--pinno-feat5-small-sub-->
						</div><!--pinno-feat5-small-wrap-->
					</div><!--pinno-feat5-main-wrap-->
				</div><!--pinno-feat5-side-out-->
				<div class="pinno-feat5-side-wrap left relative">
					<?php if(get_option('pinno_feat_ad')) { ?>
						<div class="pinno-feat1-list-ad left relative">
							<span class="pinno-ad-label"><?php esc_html_e( 'Advertisement', 'iggy-type-0' ); ?></span>
							<?php $pinno_feat_ad = get_option('pinno_feat_ad'); if ($pinno_feat_ad) { echo html_entity_decode($pinno_feat_ad); } ?>
						</div><!--pinno-feat1-list-ad-->
					<?php } ?>
					<h3 class="pinno-feat1-pop-head"><span class="pinno-feat1-pop-head"><?php echo esc_html(get_option('pinno_pop_head')); ?></span></h3>
					<div class="pinno-feat5-side-list left relative">
						<?php global $do_not_duplicate; global $post; $pop_days = esc_html(get_option('pinno_pop_days')); $popular_days_ago = "$pop_days days ago";  if(get_option('pinno_feat_ad')) { $pinno_feat_list_num = 7; } else { $pinno_feat_list_num = 10; }; $recent = new WP_Query(array('posts_per_page' => $pinno_feat_list_num, 'ignore_sticky_posts'=> 1, 'post__not_in' => $do_not_duplicate, 'orderby' => 'meta_value_num', 'order' => 'DESC', 'meta_key' => 'post_views_count', 'date_query' => array( array( 'after' => $popular_days_ago )) )); while($recent->have_posts()) : $recent->the_post(); ?>
							<a href="<?php the_permalink(); ?>" rel="bookmark">
							<div class="pinno-feat1-list-cont left relative">
								<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
									<div class="pinno-feat1-list-out relative">
										<div class="pinno-feat1-list-img left relative">
											<?php the_post_thumbnail('pinno-small-thumb'); ?>
										</div><!--pinno-feat1-list-img-->
										<div class="pinno-feat1-list-in">
											<div class="pinno-feat1-list-text">
												<div class="pinno-cat-date-wrap left relative">
													<span class="pinno-cd-cat left relative"><?php $category = get_the_category(); echo esc_html( $category[0]->cat_name ); ?></span><span class="pinno-cd-date left relative"><?php printf( esc_html__( '%s ago', 'iggy-type-0' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span>
												</div><!--pinno-cat-date-wrap-->
												<h2><?php the_title(); ?></h2>
											</div><!--pinno-feat1-list-text-->
										</div><!--pinno-feat1-list-in-->
									</div><!--pinno-feat1-list-out-->
								<?php } else { ?>
									<div class="pinno-feat1-list-text">
										<div class="pinno-cat-date-wrap left relative">
											<span class="pinno-cd-cat left relative"><?php $category = get_the_category(); echo esc_html( $category[0]->cat_name ); ?></span><span class="pinno-cd-date left relative"><?php printf( esc_html__( '%s ago', 'iggy-type-0' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span>
										</div><!--pinno-cat-date-wrap-->
										<h2><?php the_title(); ?></h2>
									</div><!--pinno-feat1-list-text-->
								<?php } ?>
							</div><!--pinno-feat1-list-cont-->
							</a>
						<?php endwhile; wp_reset_postdata(); ?>
					</div><!--pinno-feat5-side-list-->
				</div><!--pinno-feat5-side-wrap-->
			</div><!--pinno-feat5-side-out-->
		</section><!--pinno-feat5-wrap-->
	</div><!--pinno-main-box-->
<?php } else if( $pinno_feat_layout == "5" ) { ?>
	<div class="pinno-main-box">
		<section id="pinno-feat6-wrap" class="left relative">
			<?php global $do_not_duplicate; global $post; $recent = new WP_Query(array( 'tag' => get_option('pinno_feat_posts_tags'), 'posts_per_page' => '1', 'ignore_sticky_posts'=> 1 )); while($recent->have_posts()) : $recent->the_post(); $do_not_duplicate[] = $post->ID; ?>
				<a href="<?php the_permalink(); ?>" rel="bookmark">
				<div id="pinno-feat6-main" class="left relative">
					<div id="pinno-feat6-img" class="right relative">
						<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
							<?php the_post_thumbnail('pinno-post-thumb', array( 'class' => 'pinno-reg-img' )); ?>
							<?php the_post_thumbnail('pinno-port-thumb', array( 'class' => 'pinno-mob-img' )); ?>
						<?php } ?>
					</div><!--pinno-feat6-img-->
					<div id="pinno-feat6-text">
						<h3 class="pinno-feat1-pop-head"><span class="pinno-feat1-pop-head"><?php $category = get_the_category(); echo esc_html( $category[0]->cat_name ); ?></span></h3>
						<h2><?php the_title(); ?></h2>
						<p><?php echo wp_trim_words( get_the_excerpt(), 20, '...' ); ?></p>
					</div><!--pinno-feat6-text-->
				</div><!--pinno-feat6-main-->
				</a>
			<?php endwhile; wp_reset_postdata(); ?>
		</section><!--pinno-feat6-wrap-->
	</div><!--pinno-main-box-->
<?php } ?>