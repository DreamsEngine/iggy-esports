			</div><!--pinno-main-body-wrap-->
			<footer id="pinno-foot-wrap" class="left relative">
				
				<!-- ============      AGUS        ===============-->
<div class="new_footer align-items-center ">
    <div class="left_foot">
        <div id="pinno-foot-logo" class="left relative">
            <?php if (get_option('pinno_logo_foot')) { ?>
                <a href="<?php echo esc_url(home_url('/')); ?>"><img src="<?php echo esc_url(get_option('pinno_logo_foot')); ?>" alt="<?php bloginfo('name'); ?>" data-rjs="2" /></a>
            <?php } else { ?>
                <a href="<?php echo esc_url(home_url('/')); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/logos/logo-large.png" alt="<?php bloginfo('name'); ?>" data-rjs="2" /></a>
            <?php } ?>
        </div>
        <!--pinno-foot-logo-->
    </div>
    <div class="right_foot">
        <div id="pinno-foot-soc" class="left relative">
            <ul class="pinno-foot-soc-list left relative">
                <?php if (get_option('pinno_facebook')) { ?>
                    <li><a href="<?php echo esc_url(get_option('pinno_facebook')); ?>" target="_blank" class="fa fa-facebook fa-2"></a></li>
                <?php } ?>
                <?php if (get_option('pinno_twitter')) { ?>
                    <li><a href="<?php echo esc_url(get_option('pinno_twitter')); ?>" target="_blank" class="fa fa-twitter fa-2"></a></li>
                <?php } ?>
                <?php if (get_option('pinno_pinterest')) { ?>
                    <li><a href="<?php echo esc_url(get_option('pinno_pinterest')); ?>" target="_blank" class="fa fa-pinterest-p fa-2"></a></li>
                <?php } ?>
                <?php if (get_option('pinno_instagram')) { ?>
                    <li><a href="<?php echo esc_url(get_option('pinno_instagram')); ?>" target="_blank" class="fa fa-instagram fa-2"></a></li>
                <?php } ?>
                <?php if (get_option('pinno_google')) { ?>
                    <li><a href="<?php echo esc_url(get_option('pinno_google')); ?>" target="_blank" class="fa fa-google-plus fa-2"></a></li>
                <?php } ?>
                <?php if (get_option('pinno_youtube')) { ?>
                    <li><a href="<?php echo esc_url(get_option('pinno_youtube')); ?>" target="_blank" class="fa fa-youtube-play fa-2"></a></li>
                <?php } ?>
                <?php if (get_option('pinno_linkedin')) { ?>
                    <li><a href="<?php echo esc_url(get_option('pinno_linkedin')); ?>" target="_blank" class="fa fa-linkedin fa-2"></a></li>
                <?php } ?>
                <?php if (get_option('pinno_tumblr')) { ?>
                    <li><a href="<?php echo esc_url(get_option('pinno_tumblr')); ?>" target="_blank" class="fa fa-tumblr fa-2"></a></li>
                <?php } ?>
            </ul>
        </div>
        <!--pinno-foot-soc-->
    </div>
</div>
<div class="new_footer">
    <div class="left_foot">
		<div id="pinno-foot-menu"><?php wp_nav_menu(array('theme_location' => 'footer-menu')); ?></div>
		<div class="contact_hold">
			<p>Contacto editorial</p>
		<a href="mailto:prensa@theesportsmexico.com">prensa@theesportsmexico.com</a>
		</div>
    </div>
    <div class="right_foot contact_hold">
		<p>contacto comercial</p>
		<a href="mailto:contacto@theesportsmexico.com">contacto@theesportsmexico.com</a>
		<a href="<?php echo get_stylesheet_directory_uri();?>/images/esportsmx-media-kit.pdf" target="_blank">media kit</a>
    </div> 
</div>
<div id="pinno-foot-bot" class="left relative">
					<div class="pinno-main-box">
						<div id="pinno-foot-copy" class="left relative">
							<p><?php echo wp_kses_post(get_option('pinno_copyright')); ?></p>
						</div><!--pinno-foot-copy-->
					</div><!--pinno-main-box-->
				</div><!--pinno-foot-bot-->
			</footer>
		</div><!--pinno-site-main-->
	</div><!--pinno-site-wall-->
</div><!--pinno-site-->
<div class="pinno-fly-top back-to-top">
	<i class="fa fa-angle-up fa-3"></i>
</div><!--pinno-fly-top-->
<div class="pinno-fly-fade pinno-fly-but-click">
</div><!--pinno-fly-fade-->
<?php wp_footer(); ?>
</body>
</html>